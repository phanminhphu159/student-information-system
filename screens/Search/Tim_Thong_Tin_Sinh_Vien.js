import React, { Component,useState,useCallback,useMemo } from "react";
import { StyleSheet, View, Button, Picker, Alert,  ScrollView, Text, TouchableOpacity, TextInput } from "react-native";
import { Table, TableWrapper, Row, Cell } from 'react-native-table-component';
import{ Colors,StyledButtonSearch,ButtonText} from '../../components/styles'
import Xem_Thong_Tin_SV from './Xem_Thong_Tin_SV';
// import { ActivityIndicator} from 'react-native';


import GetMajor from '../../components/GetMajor';
import GetUser from '../../components/GetUser';


import axios from 'axios';
var localStorage = require('localStorage'); 

//Colors
const {tertiary,header_color} = Colors;

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
const Stack = createStackNavigator();
const Tim_Thong_Tin_Sinh_Vien = () => {
    return(
        // <NavigationContainer>   
            <Stack.Navigator
                
                screenOptions={{
                    headerStyle: {
                        backgroundColor: 'transparent'
                    },
                    headerTintColor: tertiary,
                    headerTransparent: true,
                    headerTitle: '',
                    headerLeftContainerStyle:{
                        paddingLeft: 20
                    },
                    headerLeft: null
                }}
                initialRouteName="Tim_Thong_Tin_Sinh_Vien"
            >
                <Stack.Screen name="Xem_Thong_Tin_SV" component={Xem_Thong_Tin_SV} />
                <Stack.Screen name="Tim_Thong_Tin_Sinh_Vien" component={Table_Search} />
                {/* <Stack.Screen options={{ headerTintColor: primary }} name="Welcome" component={Welcome} /> */}
            </Stack.Navigator>
        // </NavigationContainer>
    );
}

 
const Table_Search = ({navigation}) => {
  const [tableData, setTableData] = useState([]);
  const [selectedVal, setSelectedVal] = useState(null);
  const [text, setText] = useState('');
  var state = {
    tableHead: ['TT', 'Họ và tên', 'Mã SV/GV', 'Thành phố', 'Email'],
    widthArr: [40, 140, 140, 100, 200],
   }


   const getSelectedPickerValue=()=>{
    if (selectedVal == '' || selectedVal == null && text == ''){
      Alert.alert(`Vui lòng nhập Role`);
    }
    else if (text != ''){
      // console.warn(text.length);
    const token = localStorage.getItem("token");
    const USER_TOKEN = token ? "Bearer ".concat(token) : null;
    axios.defaults.headers= {Authorization: USER_TOKEN}; 
      const url1 = 'http://pbl6-point-lookup.us-east-1.elasticbeanstalk.com/api/findByUserName?username='.concat(text); 
      axios
            .get(url1)
            .then((resp)=> {
              const MyUser = resp.data;
              if ( MyUser.data == null){
                Alert.alert(`Không tồn tại`);
              }else{
              const role_student = MyUser.data.studentCode;
              var User = MyUser.data;
              var Table = [];
              Table.push(User.fullName);
              if(role_student != null){
                Table.push(User.studentCode);
              }else{
                Table.push(User.teacherCode);
              }
              Table.push(User.city);
              Table.push(User.email);
              Table.unshift('1');
              setTableData([Table]);
              }
        })
        .catch((error) => {
            // console.warn(error);
          })
    }else if(text == '') {
      const token = localStorage.getItem("token");
      const USER_TOKEN = token ? "Bearer ".concat(token) : null;
      if (selectedVal == 'STUDENT'){
            axios.defaults.headers= {Authorization: USER_TOKEN}; 
            const url = 'http://pbl6-point-lookup.us-east-1.elasticbeanstalk.com/api/listPerson?roleCode='.concat(selectedVal); 
            axios
                  .get(url)
                  .then((resp)=> {
                    const MyArr = resp.data;
                    var Table = MyArr.data.map(record=>([record.fullName, record.studentCode,record.city,record.email]));
                    for (let i = 0; i < Table.length; i += 1) {
                      Table[i].unshift(i + 1);
                    }
                    setTableData(Table);
                    // console.warn(Table);
              })
              .catch((error) => {
                  // console.warn(error);
                })
          }else{
            axios.defaults.headers= {Authorization: USER_TOKEN}; 
            const url = 'http://pbl6-point-lookup.us-east-1.elasticbeanstalk.com/api/listPerson?roleCode='.concat(selectedVal); 
            axios
                  .get(url)
                  .then((resp)=> {
                    const MyArr = resp.data;
                    var Table = MyArr.data.map(record=>([record.fullName, record.teacherCode,record.city,record.email]));
                    for (let i = 0; i < Table.length; i += 1) {
                      Table[i].unshift(i + 1);
                    }
                    setTableData(Table);
                    // console.warn(Table);
              })
              .catch((error) => {
                  // console.warn(error);
                })
            }
      }
  }

    const _alertIndex=(index) => {
      Alert.alert(`This is row ${index + 1}`);
    }

 
    const element = (data, index) => (
        <TouchableOpacity onPress={() => {
            _alertIndex(index);
            navigation.navigate("Xem_Thong_Tin_SV",{
              test: index + 1,
            });
        }}>
          <View style={styles.btn}>
            <Text style={styles.btnText}>Xem TT</Text>
          </View>
        </TouchableOpacity>
      );


    return (
      <View style={styles.container}>

      <View style={styles.card}>
        <Text style={{fontSize: 18,textAlign: "left"}}> Role : </Text>
        <View style={styles.container_dropdown}>
        <Picker
            style={{ height: 30 }}
            itemStyle={{ backgroundColor: "grey", color: "blue", fontFamily:"Ebrima", fontSize:17 }}
            selectedValue={selectedVal}
            onValueChange={(itemValue, itemIndex) => setSelectedVal(itemValue)} >
            <Picker.Item label='' value=''/>
            <Picker.Item label='Sinh Viên' value='STUDENT'/>
            <Picker.Item label='Giáo viên' value='TEACHER'/>
        </Picker>
        </View>
        </View>


        {/* Tên */}
        {/* <View style={styles.card}>
            <Text style={{fontSize: 18,textAlign: "left"}}> Mã SV/GV   : </Text>
            <View style={styles.container_dropdown}>
                <TextInput style={styles.textinput} 
                  placeholder=""
                  onChangeText={text => setText(text)}
                  // defaultValue={text}
                > 
                <Text >{text.split(' ')}</Text>
                </TextInput>
            </View>
        </View> */}
        {/* Button */}
        <View style={{paddingTop: 10}} />


        <StyledButtonSearch  onPress={ getSelectedPickerValue}>
        <ButtonText>Tìm Kiếm</ButtonText>
        </StyledButtonSearch>

        {/* Text */}
        <View style={{paddingTop: 20}} />
        <Text style={{fontSize: 18}}> Danh sách tìm kiếm </Text>

        <ScrollView horizontal={true}>
            <View style={{height: 400}}> 

            <Table borderStyle={{borderWidth: 1, borderColor: '#C1C0B9'}} style={{paddingTop:5}}>
              <Row data={state.tableHead} widthArr={state.widthArr} style={styles.header} textStyle={styles.text}/>
            </Table>
            <ScrollView style={styles.dataWrapper} >
              <Table 
                borderStyle={{borderWidth: 1, borderColor: '#C1C0B9'}} 
                // style={{height:300, flex: 1}} 
              >
                {   
                  tableData.map((rowData, index) => (
                    <Row
                      key={index}
                      data = {
                          rowData.map((cellData, cellIndex) => (
                            <Cell key={cellIndex} data={cellData} textStyle={styles.text} style={{alignItems:"center"}}/>
                          ))
                      }
                      widthArr={state.widthArr}
                      style={[styles.row, index%2 && {backgroundColor: '#F7F6E7'}]}
                      textStyle={styles.text}
                    />
                  ))
                }
              </Table>
            {/* <View style={{paddingBottom: 60}}></View> */}
            </ScrollView>
          </View>
        </ScrollView>
      </View>
    )
}
 

const styles = StyleSheet.create({
  container: { flex: 1, padding: 16, paddingTop: 13, backgroundColor: '#fff' },
  header: { height: 50, backgroundColor: header_color },
  text: { textAlign: 'center', fontWeight: '100' },
  textinput:{fontSize: 17, textAlign: "auto", paddingLeft: 8},
  dataWrapper: { marginTop: -1 },
  row: { height: 40, backgroundColor: '#E7E6E1' },
  btn: { width: 70, height: 20, backgroundColor: '#78B7BB',  borderRadius: 2 },
  btnText: { textAlign: 'center', color: '#fff' },
  container_dropdown: {
    flex:1,
    borderWidth: 1,
    borderColor: "blue",
    borderRadius: 10,
  },
  card: {
    alignItems:'center',
    flexDirection: 'row',
    paddingBottom: 10
  },
});



export default Tim_Thong_Tin_Sinh_Vien;


// export default HomeActivity;
// export default Chuong_Trinh_Dao_Tao;