import React, { Component } from 'react';


import {SafeAreaView, Text, View} from 'react-native';
import{ StyledContainer, InnerContainer, PageLogo, PageTitle, SubTitle, StyledFormArea, LeftIcon, StyledInputLabel, StyledTextInput, RightIcon, StyledButton, ButtonText, Colors, MsgBox, Line, ExtraView, ExtraText, TextLink, TextLinkContent} from '../../components/styles'


class Xem_Thong_Tin_SV extends Component {

    constructor(props) {
        super(props);
        this.state = {
          tableHead: ['TT', 'Khoa quản lý', 'Lớp', 'Số lượng sinh viên', 'Xem danh sách '],
          widthArr: [40, 100, 80, 160, 120],
          PickerSelectedVal : ''
        }
    }

    render() {
        {/*Using the navigation prop we can get the 
              value passed from the previous screen*/}  
        const { navigation } = this.props;  
        // const test1 = navigation.getParam('test', 'some default value');  
        const test1 = this.props.route.params.test

        return(
            <View>
            <Text>Test: {JSON.stringify(test1)}
                Thông tin sinh viên chi tiết sẽ xuất hiện ở đây
                </Text>
                <StyledButton onPress={() => navigation.navigate("Tim_Thong_Tin_Sinh_Vien")}>
                    <ButtonText>Go Back</ButtonText>
                </StyledButton>
            </View> 
        );
    }
};

export default Xem_Thong_Tin_SV;