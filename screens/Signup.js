import React, {useState} from 'react';
import { StatusBar } from 'expo-status-bar';

// create components
import {Formik, formik} from 'formik';
import {Octicons, Ionicons, Fontisto} from '@expo/vector-icons';
import {View, TouchableOpacity} from 'react-native';
import{ StyledContainer, InnerContainer, PageLogo, PageTitle, SubTitle, StyledFormArea, LeftIcon, StyledInputLabel, StyledTextInput, RightIcon, StyledButton, ButtonText, Colors, MsgBox, Line, ExtraView, ExtraText, TextLink, TextLinkContent} from './../components/styles'

//Colors
const {brand, darkLight, primary} = Colors;

// Datetimepicker
import DateTimePicker from '@react-native-community/datetimepicker';

// keyboard avoiding view
import KeyboardAvoidingWrapper from './../components/KeyboardAvoidingWrapper';

const Signup = ({navigation}) => {
    // useState — const [state, setState] = useState(initialState);. Returns a stateful value, and a function to update it. During the initial render, the returned 
    //
    const [hidePassword , setHidePassword] = useState(true);
    const [show, setShow] = useState(false);
    const [date, setDate] = useState(new Date(2000, 0, 1))

    const [dob, setDob] = useState();

    const onChange = (event, selectedDate) => {
        const currenDate = selectedDate || date;
        setShow(false);
        setDate(currenDate);
        setDob(currenDate);
        }
    
    const ShowDatePicker = () => {
        setShow(true);
    }
    
    return(
        <KeyboardAvoidingWrapper>
        <StyledContainer>
            <StatusBar style="dark" />
            <InnerContainer>
                <PageTitle> Phu  </PageTitle>
                <SubTitle>Account Signup</SubTitle>
            
                {show && (
                    <DateTimePicker 
                        testID="dateTimePicker"
                        value = {date}
                        mode = 'date'
                        is24Hour={true}
                        display="default"
                        onChange = {onChange}   
                    />
                )}

                <Formik
                    initialValues = {{fullName: '', email: '', dateOfBirth: '', password: '', confirmPassword: ''}}
                    onSubmit = {(values) => {
                        console.log(values);
                        navigation.navigate('Welcome');
                    }}
                >
                    {({handleChange, handleBlur, handleSubmit, values}) => (<StyledFormArea>
                        <MyTextInPut
                            label="Full name"
                            icon= "person"
                            placeholder= "Richard Barnes"
                            placeholderTextColor= {darkLight}
                            onChangeText = {handleChange('fullName')}
                            onBlur = {handleBlur('fullName')}
                            value={values.fullName}
                        />

                        <MyTextInPut
                            label="Email Address"
                            icon= "mail"
                            placeholder= "abc@gmail.com"
                            placeholderTextColor= {darkLight}
                            onChangeText = {handleChange('email')}
                            onBlur = {handleBlur('email')}
                            value={values.email}
                            keyboardType = "email-address"
                        />

                        <MyTextInPut
                            label="Date of Birth"
                            icon= "calendar"
                            placeholder= "YYYY - MM - DD"
                            placeholderTextColor= {darkLight}
                            onChangeText = {handleChange('dateOfBirth')}
                            onBlur = {handleBlur('dateOfBirth')}
                            value={dob ? dob.toDateString() : ''}
                            isDate={true}
                            editable = {false}
                            ShowDatePicker={ShowDatePicker}
                        />

                        <MyTextInPut
                            label="Password"
                            icon= "lock"
                            placeholder= "* * * * * * * * * *"
                            placeholderTextColor= {darkLight}
                            onChangeText = {handleChange('password')}
                            onBlur = {handleBlur('password')}
                            value={values.password}
                            secureTextEntry={hidePassword}
                            isPassword={true}
                            hidePassword={hidePassword}
                            setHidePassword={setHidePassword}
                        />

                        <MyTextInPut
                            label="Confirm Password"
                            icon= "lock"
                            placeholder= "* * * * * * * * * *"
                            placeholderTextColor= {darkLight}
                            onChangeText = {handleChange('confirmPassword')}
                            onBlur = {handleBlur('confirmPassword')}
                            value={values.confirmPassword}
                            secureTextEntry={hidePassword}
                            isPassword={true}
                            hidePassword={hidePassword}
                            setHidePassword={setHidePassword}
                        />
                        <MsgBox>...</MsgBox>
                        <StyledButton onPress={handleSubmit}>
                            <ButtonText>Signup</ButtonText>
                        </StyledButton>
                        <Line />
                        <ExtraView>
                            <ExtraText> Already have an account?</ExtraText>
                            <TextLink onPress={() => navigation.navigate('Login')}>
                                <TextLinkContent> Login</TextLinkContent>
                            </TextLink>
                        </ExtraView>

                    </StyledFormArea>)}
                </Formik>
            </InnerContainer>
        </StyledContainer>
        </KeyboardAvoidingWrapper>
    );
};

const MyTextInPut = ({label, icon, isPassword, hidePassword, setHidePassword, isDate, ShowDatePicker, ...props}) =>{
    return(
        <View>
            <LeftIcon>
                <Octicons name={icon} size={30} color={brand} />
            </LeftIcon>
            <StyledInputLabel>{label}</StyledInputLabel>
            {!isDate && <StyledTextInput {...props} />}
            {isDate && (
                <TouchableOpacity onPress={ShowDatePicker}>
                    <StyledTextInput {...props} />
                </TouchableOpacity>
            )}
            {isPassword && (
                <RightIcon onPress={() => setHidePassword(!hidePassword)}>
                    <Ionicons name={hidePassword ? 'md-eye-off' : 'md-eye'}  size={30} color={darkLight} />
                </RightIcon>
            )}
        </View>
    );
}

export default Signup;