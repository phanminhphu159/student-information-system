import React, { Component } from 'react';


import { Table, TableWrapper, Row, Cell } from 'react-native-table-component';
import { StyleSheet, View, Button, Picker, Alert,  ScrollView, Text, TouchableOpacity } from "react-native";
import{ StyledContainer, InnerContainer, PageLogo, PageTitle, SubTitle, StyledFormArea, LeftIcon, StyledInputLabel, StyledTextInput, RightIcon, StyledButton, ButtonText, Colors, MsgBox, Line, ExtraView, ExtraText, TextLink, TextLinkContent} from 'D:/React native/Phu_test/components/styles'


class Xem_Danh_Sach_SV_Trong_HP extends Component {

    constructor(props) {
        super(props);
        this.state = {
            tableHead: ['TT', 'Họ tên', 'Lớp', 'Khoa', 'Công thức điểm', 'Điểm BT', 'Điểm GK', 'Điểm CK', 'Thang 10', 'Thang 4','Điểm chữ', 'Xem  TT', 'Sửa điểm', 'Xoá'],
            widthArr: [40, 100, 160, 160,180, 80, 80, 80, 80, 80, 80,80,80, 80],
            PickerSelectedVal : ''
        }
    }

    getSelectedPickerValue=()=>{
        Alert.alert("Selected country is : " +this.state.PickerSelectedVal);
      }
    
      _alertIndex(index) {
        Alert.alert(`This is row ${index + 1}`);
      }

    render() {
        {/*Using the navigation prop we can get the 
              value passed from the previous screen*/} 
        const state = this.state; 
        const { navigation } = this.props;  
        const element = (data, index) => (
            <TouchableOpacity onPress={() => {
                this._alertIndex(index);
                // this.props.navigation.navigate("Xem_Danh_Sach_SV_Trong_HP",{
                //   test: index + 1,
                // });
            }}>
              <View style={styles.btn}>
                <Text style={styles.btnText}>Xem TT</Text>
              </View>
            </TouchableOpacity>
          );

          
        const Edit = (data, index) => (
            <TouchableOpacity onPress={() => {
                this._alertIndex(index);
                // this.props.navigation.navigate("Xem_Danh_Sach_SV_Trong_HP",{
                //   test: index + 1,
                // });
            }}>
              <View style={styles.btn}>
                <Text style={styles.btnText}>Sửa điểm</Text>
              </View>
            </TouchableOpacity>
          );
        const tableData = [];
          for (let i = 0; i < 12; i += 1) {
            const rowData = [];
            for (let j = 0; j < 13; j += 1) {
              rowData.push(`${i}${j}`);
            }
            tableData.push(rowData);
        }
        // const test1 = navigation.getParam('test', 'some default value');  
        const test1 = this.props.route.params.test

        return(
            // <View>
            // <Text>Test: {JSON.stringify(test1)}
            //     Danh sách tin sinh viên trong học phần sẽ xuất hiện ở đây
            //     </Text>
            //     <StyledButton onPress={() => navigation.navigate("Lich_Day_Hoc")}>
            //         <ButtonText>Go Back</ButtonText>
            //     </StyledButton>
            // </View> 
            <View style={styles.container}>


            <View style={{paddingTop: 5}}></View>
            <Text style={{fontSize: 18}}> Danh sách sinh viên </Text>
            <ScrollView horizontal={true}>
                <View style={{height: 420}}> 
                <Table borderStyle={{borderWidth: 1, borderColor: '#C1C0B9'}} style={{paddingTop:5}}>
                  <Row data={state.tableHead} widthArr={state.widthArr} style={styles.header} textStyle={styles.text}/>
                </Table>
                <ScrollView style={styles.dataWrapper} >
                  <Table 
                    borderStyle={{borderWidth: 1, borderColor: '#C1C0B9'}} 
                    // style={{height:300, flex: 1}} 
                  >
                    {   
                      tableData.map((rowData, index) => (
                        <Row
                          key={index}
                          data = {
                              rowData.map((cellData, cellIndex) => (
                                <>
                                <Cell key={cellIndex} data={cellIndex === 11 ? element(cellData, index) : cellIndex === 12 ? Edit(cellData, index) : cellData } textStyle={styles.text} style={{alignItems:"center"}}/> 
                                {/* <Cell key={cellIndex} data={cellIndex === 12 ? element(cellData, index) : cellData } textStyle={styles.text} style={{alignItems:"center"}}/>  */}
                                </>
                              ))
                          }
                          widthArr={state.widthArr}
                          style={[styles.row, index%2 && {backgroundColor: '#F7F6E7'}]}
                          textStyle={styles.text}
                        />
                      ))
                    }
                  </Table>
                {/* <View style={{paddingBottom: 60}}></View> */}
                </ScrollView>
              </View>
            </ScrollView>

            <View style={{paddingTop: 10}}></View>
            <StyledButton onPress={() => navigation.navigate("Lich_Day_Hoc")}>
            <ButtonText>Go Back</ButtonText>
            </StyledButton>
            <Text>Test: {JSON.stringify(test1)}
                Danh sách tin sinh viên trong học phần sẽ xuất hiện ở đây
            </Text>
        </View>
        );
    }
};


const styles = StyleSheet.create({
    container: { flex: 0, padding: 16, paddingTop: 13, backgroundColor: '#fff' },
    header: { height: 50, backgroundColor: '#537791' },
    text: { textAlign: 'center', fontWeight: '100' },
    dataWrapper: { marginTop: -1 },
    row: { height: 40, backgroundColor: '#E7E6E1' },
    btn: { width: 70, height: 20, backgroundColor: '#78B7BB',  borderRadius: 2 },
    btnText: { textAlign: 'center', color: '#fff' },
    container_dropdown: {
      flex:1,
      borderWidth: 1,
      borderColor: "blue",
      borderRadius: 10,
    },
    card: {
      alignItems:'center',
      flexDirection: 'row',
      paddingBottom: 10
    },
});

export default Xem_Danh_Sach_SV_Trong_HP;