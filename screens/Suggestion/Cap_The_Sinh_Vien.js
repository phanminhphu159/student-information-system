import React, { Component } from "react";
import { StyleSheet, View, Button, Picker, Alert,  ScrollView, Text, TouchableOpacity } from "react-native";
import { Table, TableWrapper, Row, Cell } from 'react-native-table-component';
import{ Colors} from '../../components/styles'


//Colors
const {tertiary} = Colors;


// class Dropdown extends Component {

//   constructor(){
//      super();
//      this.state={
//        PickerSelectedVal : ''
//      }
//    }

//    getSelectedPickerValue=()=>{
//       Alert.alert("Selected country is : " +this.state.PickerSelectedVal);
//     }

//     _alertIndex(index) {
//         Alert.alert(`This is row ${index + 1}`);
//       }


//   render() {
//     return (
//       <View style={styles.container_dropdown}>
//         <Picker
//            selectedValue={this.state.PickerSelectedVal}
//            onValueChange={(itemValue, itemIndex) => this.setState({PickerSelectedVal: itemValue})} >

//            <Picker.Item label="India" value="India" />
//            <Picker.Item label="USA" value="USA" />
//            <Picker.Item label="China" value="China" />
//            <Picker.Item label="Russia" value="Russia" />
//            <Picker.Item label="United Kingdom" value="United Kingdom" />
//            <Picker.Item label="France" value="France" />
//         </Picker>

//          <Button title="Get Selected Picker Value" onPress={ this.getSelectedPickerValue } />
//       </View>
//     );
//   }
  
// }

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
const Stack = createStackNavigator();
const Cap_The_Sinh_Vien = () => {
    return(
        // <NavigationContainer>   
            <Stack.Navigator
                
                screenOptions={{
                    headerStyle: {
                        backgroundColor: 'transparent'
                    },
                    headerTintColor: tertiary,
                    headerTransparent: true,
                    headerTitle: '',
                    headerLeftContainerStyle:{
                        paddingLeft: 20
                    },
                    headerLeft: null,
                }}
                initialRouteName="Tim_Kiem_Khoa_Lop"
            >
                {/* <Stack.Screen name="Xem_Danh_Sach_Lop" component={Xem_Danh_Sach_Lop} /> */}
                <Stack.Screen name="Tim_Kiem_Khoa_Lop" component={Table_Search} />
                {/* <Stack.Screen options={{ headerTintColor: primary }} name="Welcome" component={Welcome} /> */}
            </Stack.Navigator>
        // </NavigationContainer>
    );
}

 
class Table_Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tableHead: ['TT', 'Tên hồ sơ đề nghị', 'Đề nghị lúc', 'Đã cấp'],
      widthArr: [40, 180, 100, 100],
      PickerSelectedVal : ''
    }
  }


  getSelectedPickerValue=()=>{
    Alert.alert("Selected country is : " +this.state.PickerSelectedVal);
  }

  _alertIndex(index) {
    Alert.alert(`This is row ${index + 1}`);
  }

 
  render() {
    const state = this.state;
    const { navigate } = this.props.navigation;
    const element = (data, index) => (
        <TouchableOpacity onPress={() => {
            this._alertIndex(index);
            // this.props.navigation.navigate("Xem_Danh_Sach_Lop",{
            //   test: index + 1,
            // });
        }}>
          <View style={styles.btn}>
            <Text style={styles.btnText}>Xem </Text>
          </View>
        </TouchableOpacity>
      );
    const tableData = [];
    const rowData10 = ['1','Thẻ sinh viên','10/12/2021',''];
    const rowData11 = ['2','Thẻ sinh viên','11/12/2021',''];
    tableData.push(rowData10);
    tableData.push(rowData11);

    const state1 = {
      tableHead: ['Mã chương trình', 'Tên chương trình đào tạo', 'Số học kỳ', 'Tổng số tín chỉ yêu cầu', 'Số tín chỉ bắt buộc', 'Số tín chỉ tự chọn', 'Mã ngành'],
      widthArr: [140, 220, 120, 160, 140, 140, 140]
    }
    const rowData1 = ['1021074','Công nghệ Thông tin K2018CLC Đặc thù_HTTT','8','120','113','7','7480201'];
    const tableData1 = [];
    tableData1.push(rowData1);


    return (
      <View style={styles.container}>

        <View style={styles.card}>
        <Text style={{fontSize: 18,textAlign: "left"}}> Loại thẻ : </Text>
        <View style={styles.container_dropdown}>
        <Picker
            style={{ height: 30 }}
            itemStyle={{ backgroundColor: "grey", color: "blue", fontFamily:"Ebrima", fontSize:17 }}
            selectedValue={this.state.PickerSelectedVal}
            onValueChange={(itemValue, itemIndex) => this.setState({PickerSelectedVal: itemValue})} >

            <Picker.Item label="Thẻ sinh viên" value="India" />
            <Picker.Item label="Tài khoản" value="India" />
        </Picker>
        </View>
        </View>
        {/* <Button s title="Get Selected Picker Value" onPress={ this.getSelectedPickerValue } /> */}

        <View style={{paddingTop: 20}}></View>
        <Text style={{fontSize: 18}}> Danh sách các đơn đề nghị đã lập </Text>
        <ScrollView horizontal={true}>
            <View style={{height: 460}}> 
            <Table borderStyle={{borderWidth: 1, borderColor: '#C1C0B9'}} style={{paddingTop:5}}>
              <Row data={state.tableHead} widthArr={state.widthArr} style={styles.header} textStyle={styles.text}/>
            </Table>
            <ScrollView style={styles.dataWrapper} >
              <Table 
                borderStyle={{borderWidth: 1, borderColor: '#C1C0B9'}} 
                // style={{height:300, flex: 1}} 
              >
                {   
                  tableData.map((rowData, index) => (
                    <Row
                      key={index}
                      data = {
                          rowData.map((cellData, cellIndex) => (
                            <Cell key={cellIndex} data={ cellData} textStyle={styles.text} style={{alignItems:"center"}}/>
                          ))
                      }
                      widthArr={state.widthArr}
                      style={[styles.row, index%2 && {backgroundColor: '#F7F6E7'}]}
                      textStyle={styles.text}
                    />
                  ))
                }
              </Table>
            {/* <View style={{paddingBottom: 60}}></View> */}
            </ScrollView>
          </View>
        </ScrollView>
      </View>
    )
  }
}
 

const styles = StyleSheet.create({
  container: { flex: 1, padding: 16, paddingTop: 13, backgroundColor: '#fff' },
  header: { height: 50, backgroundColor: '#537791' },
  text: { textAlign: 'center', fontWeight: '100' },
  dataWrapper: { marginTop: -1 },
  row: { height: 40, backgroundColor: '#E7E6E1' },
  btn: { width: 70, height: 20, backgroundColor: '#78B7BB',  borderRadius: 2 },
  btnText: { textAlign: 'center', color: '#fff' },
  container_dropdown: {
    flex:1,
    borderWidth: 1,
    borderColor: "blue",
    borderRadius: 10,
  },
  card: {
    alignItems:'center',
    flexDirection: 'row',
    paddingBottom: 10
  },
});


// const Tim_Kiem_Khoa_Lop1 = ({props}) => {
//     return(
//         <>
//             <Table_Search></Table_Search>
//         </>
//     );
// };

export default Cap_The_Sinh_Vien;


// export default HomeActivity;
// export default Chuong_Trinh_Dao_Tao;