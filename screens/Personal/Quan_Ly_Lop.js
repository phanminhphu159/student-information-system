import React, { Component,useState } from "react";
import { StyleSheet, View, Button, Picker, Alert,  ScrollView, Text, TouchableOpacity } from "react-native";
import { Table, TableWrapper, Row, Cell } from 'react-native-table-component';
import{ Colors,StyledButtonSearch,ButtonText} from '../../components/styles'

import GetMajor from '../../components/GetMajor';
import GetClassFromMajor from '../../components/GetClassFromMajor';
import Them_lop from '../../screens/Personal/Them_lop';
import Xem_Danh_Sach_SV_Trong_Lop from '../../screens/Personal/Xem_Danh_Sach_SV_Trong_Lop';

import axios from 'axios';
var localStorage = require('localStorage');
//Colors
const {tertiary,header_color} = Colors;

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
const Stack = createStackNavigator();
const Tim_Kiem_Khoa_Lop = () => {
    return(
        // <NavigationContainer>   
            <Stack.Navigator
                
                screenOptions={{
                    headerStyle: {
                        backgroundColor: 'transparent'
                    },
                    headerTintColor: tertiary,
                    headerTransparent: true,
                    headerTitle: '',
                    headerLeftContainerStyle:{
                        paddingLeft: 20
                    },
                    headerLeft: null,
                }}
                initialRouteName="Quan_Ly_Lop"
            >
                <Stack.Screen name="Them_lop" component={Them_lop} />
                <Stack.Screen name="Quan_Ly_Lop" component={Table_Search} />
                <Stack.Screen name="Xem_Danh_Sach_SV_Trong_Lop" component={Xem_Danh_Sach_SV_Trong_Lop} />
                {/* <Stack.Screen options={{ headerTintColor: primary }} name="Welcome" component={Welcome} /> */}
            </Stack.Navigator>
        // </NavigationContainer>
    );
}

 


const Table_Search = ({navigation}) => {
  const [selectedVal, setSelectedVal] = useState(null);
  const [selectedItem, setSelectedItem] = useState(null);
  const [tableData, setTableData] = useState([]);
  const state = {
    tableHead: ['TT','Tên Lớp', 'Mã Lớp', 'Xem danh sách '],
    widthArr: [40,260, 100, 120],
   }

  // Khoa Picker:
  const Major = GetMajor();
  var PickerMajor = null;
  if (Major != null){
    PickerMajor = Major.data.map((item) => {return item;})
  }


  const getSelectedPickerValue=()=>{
    if (selectedVal == null || selectedVal == ''){
      Alert.alert("Vui lòng nhập khoa");
    }
    else{
    const token = localStorage.getItem("token");
    const USER_TOKEN = token ? "Bearer ".concat(token) : null;
    axios.defaults.headers= {Authorization: USER_TOKEN}; 
      const url = 'http://pbl6-point-lookup.us-east-1.elasticbeanstalk.com/api/findClassByMajor?majorCode='.concat(selectedVal); 
      axios
            .get(url)
            .then((resp)=> {
              const MyArr = resp.data;
              if(MyArr.data == null){
                Alert.alert("Không có lớp nào");
              }else{
              var Table = MyArr.data.map(record=>([record.className, record.classCode]));
              for (let i = 0; i < Table.length; i += 1) {
                Table[i].push("");
              }
              for (let i = 0; i < Table.length; i += 1) {
                Table[i].unshift(i + 1);
              }
              setTableData(Table);
            }
        })
        .catch((error) => {
            // console.warn(error);
          })
    } 
  }

  const Addclass=() => {
    if (selectedVal == null || selectedVal == ''){
        Alert.alert("Vui lòng nhập khoa");
    }else{
    navigation.navigate('Them_lop',{
        majorcode: selectedItem
    });
          
      }
  }

 
  const element = (data, index) => (
    <TouchableOpacity onPress={() => {
        navigation.navigate("Xem_Danh_Sach_SV_Trong_Lop",{
          classcode: tableData[index][2],
        });
    }}>
      <View style={styles.btn}>
        <Text style={styles.btnText}>Xem DS</Text>
      </View>
    </TouchableOpacity>
  );

    return (
      <View style={styles.container}>

        <View style={styles.card}>
        <Text style={{fontSize: 18,textAlign: "left"}}> Khoa : </Text>
        <View style={styles.container_dropdown}>
        <Picker
            style={{ height: 30 }}
            itemStyle={{ backgroundColor: "grey", color: "blue", fontFamily:"Ebrima", fontSize:17 }}
            selectedValue={selectedVal}
            onValueChange={(itemValue, itemIndex) => {setSelectedVal(itemValue); setSelectedItem(PickerMajor.map(item => item.majorName)[itemIndex-1])}} >
            <Picker.Item label='' value=''/>
            { PickerMajor != null ? 
              PickerMajor.map((item, index) => {
              return (<Picker.Item label={item.majorName} value={item.majorCode} key={index}/>) }) : <Picker.Item label="" value=""/>}
        </Picker>
        </View>
        </View>
        <StyledButtonSearch  onPress={ getSelectedPickerValue}>
        <ButtonText>Tìm Kiếm</ButtonText>
        </StyledButtonSearch>

        <View style={{paddingTop: 20}}></View>
        <Text style={{fontSize: 18}}> Danh sách khoa, lớp </Text>
        <ScrollView horizontal={true}>
            <View style={{height: 327}}> 
            <Table borderStyle={{borderWidth: 1, borderColor: '#C1C0B9'}} style={{paddingTop:5}}>
              <Row data={state.tableHead} widthArr={state.widthArr} style={styles.header} textStyle={styles.text}/>
            </Table>
            <ScrollView style={styles.dataWrapper} >
              <Table 
                borderStyle={{borderWidth: 1, borderColor: '#C1C0B9'}} 
                // style={{height:300, flex: 1}} 
              >
                {   
                  tableData.map((rowData, index) => (
                    <Row
                      key={index}
                      data = {
                          rowData.map((cellData, cellIndex) => (
                            <Cell key={cellIndex} data={cellIndex === 3 ? element(cellData, index) : cellData} textStyle={styles.text} style={{alignItems:"center"}}/>
                          ))
                      }
                      widthArr={state.widthArr}
                      style={[styles.row, index%2 && {backgroundColor: '#F7F6E7'}]}
                      textStyle={styles.text}
                    />
                  ))
                }
              </Table>
            {/* <View style={{paddingBottom: 60}}></View> */}
            </ScrollView>
          </View>
        </ScrollView>

        <View style={{paddingBottom:60}}>
        <StyledButtonSearch  style={{paddingBottom: 20}} 
        onPress={Addclass}>
        <ButtonText>Thêm lớp</ButtonText>
         </StyledButtonSearch>
        </View>
      </View>
    )
}
 

const styles = StyleSheet.create({
  container: { flex: 1, padding: 16, paddingTop: 13, backgroundColor: '#fff' },
  header: { height: 50, backgroundColor: header_color },
  text: { textAlign: 'center', fontWeight: '100' },
  dataWrapper: { marginTop: -1 },
  row: { height: 40, backgroundColor: '#E7E6E1' },
  btn: { width: 70, height: 20, backgroundColor: '#78B7BB',  borderRadius: 2 },
  btnText: { textAlign: 'center', color: '#fff' },
  container_dropdown: {
    flex:1,
    borderWidth: 1,
    borderColor: "blue",
    borderRadius: 10,
  },
  card: {
    alignItems:'center',
    flexDirection: 'row',
    paddingBottom: 10
  },
});



export default Tim_Kiem_Khoa_Lop;

