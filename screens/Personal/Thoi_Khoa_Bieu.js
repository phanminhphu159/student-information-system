import { useBottomTabBarHeight } from '@react-navigation/bottom-tabs';
import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, Text } from 'react-native';
import { Table, TableWrapper, Row } from 'react-native-table-component';
 
export default class Thoi_Khoa_Bieu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tableHead: ['TT', 'Mã lớp học phần', 'Tên lớp học phần', 'Nhóm thi', 'Lịch thi cuối kỳ'],
      widthArr: [40, 160, 180, 180, 180]
    }
  }
 
  render() {
    const state = this.state;
    const tableData = [];
    for (let i = 0; i < 15; i += 1) {
      const rowData = [];
      for (let j = 0; j < 9; j += 1) {
        rowData.push(`${i}${j}`);
      }
      tableData.push(rowData);
    }

    const state1 = {
      tableHead: ['TT','Mã lớp học phần', 'Tên lớp học phần', 'Số TC', 'Giảng viên', 'Thời khóa biểu', 'Tuần học', 'Công thức điểm'],
      widthArr: [40, 140, 140, 120, 160, 140, 140, 180]
    }
    const rowData1 = ['1','1022013.2110.18.14','Cơ sở HT thông tin','2','Nguyễn Văn Hiệu','Thứ 4,7-9,MST','6-16','[GK]*0.20+[BT]*0.20+[CK]*0.60'];
    const tableData1 = [];
    tableData1.push(rowData1);
    tableData1.push(rowData1);
    tableData1.push(rowData1);
    tableData1.push(rowData1);
    tableData1.push(rowData1);
    tableData1.push(rowData1);
    tableData1.push(rowData1);
    tableData1.push(rowData1);
    tableData1.push(rowData1);
    tableData1.push(rowData1);
    tableData1.push(rowData1);
    tableData1.push(rowData1);


    return (
      <View style={styles.container}>
        {/* Bảng 1 */}
        <View style={{paddingTop: 0}}/>
        <Text style={{fontSize: 18, paddingBottom: 5}}> Lịch học </Text>
        <ScrollView horizontal={true}>
          <View style={{height: 251}}>
            <Table borderStyle={{borderWidth: 1, borderColor: '#C1C0B9'}}>
              <Row data={state1.tableHead} widthArr={state1.widthArr} style={styles.header} textStyle={styles.text}/>
            </Table>
            <ScrollView style={styles.dataWrapper}>
            <Table borderStyle={{borderWidth: 1, borderColor: '#C1C0B9'}} >
            {
              tableData1.map((rowData1, index) => (
                <Row
                  key={index}
                  data={rowData1}
                  widthArr={state1.widthArr}
                  style={[styles.row, index%2 && {backgroundColor: '#F7F6E7'}]}
                  textStyle={styles.text}
                  />
              ))
            }
            </Table>
            </ScrollView>
          </View> 
        </ScrollView>

        {/* Bảng 2 */}
        <View style={{paddingTop: 20}}/>
        <Text style={{fontSize: 18}}> Lịch Thi </Text>
        <ScrollView horizontal={true} >
          <View style={{height: 256}}>
            <Table borderStyle={{borderWidth: 1, borderColor: '#C1C0B9'}} style={{paddingTop:5}}>
              <Row data={state.tableHead} widthArr={state.widthArr} style={styles.header} textStyle={styles.text}/>
            </Table>
            <ScrollView style={styles.dataWrapper}>
              <Table borderStyle={{borderWidth: 1, borderColor: '#C1C0B9'}} >
                {
                  tableData.map((rowData, index) => (
                    <Row
                      key={index}
                      data={rowData}
                      widthArr={state.widthArr}
                      style={[styles.row, index%2 && {backgroundColor: '#F7F6E7'}]}
                      textStyle={styles.text}
                    />
                  ))
                }
              </Table>
          </ScrollView>
          </View>
        </ScrollView>
      </View>
    )
  }
}
 
const styles = StyleSheet.create({
  container: { flex: 0, padding: 16, paddingTop: 0, backgroundColor: '#fff' },
  header: { height: 50, backgroundColor: '#537791' },
  text: { textAlign: 'center', fontWeight: '100' },
  dataWrapper: { marginTop: -1 },
  row: { height: 40, backgroundColor: '#E7E6E1' }
});