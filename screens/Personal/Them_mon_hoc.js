
import React, {useState,useEffect} from 'react';
import { StatusBar } from 'expo-status-bar';

// create components
import {Formik, formik} from 'formik';
import {Octicons, Ionicons, Fontisto} from '@expo/vector-icons';
import {View, ScrollView,ActivityIndicator} from 'react-native';
import{ StyledContainer, RightIcon1, InnerContainer, PageLogo, PageTitle, SubTitle, StyledFormArea, LeftIcon, StyledInputLabel, StyledInputLabel_1 , StyledTextInput, StyledTextInput_1, RightIcon, 
    StyledButton, ButtonText, Colors, MsgBox,MsgBox1, Line, ExtraView, ExtraText, TextLink, TextLinkContent, WelcomeContainer, TTSV_Container, WelcomeImage, Avatar, AvatarContainer,StyledButtonSearch} from '../../components/styles';


import { StackActions } from '@react-navigation/native';

// API
import axios from 'axios';
axios.defaults.baseURL = 'http://pbl6-point-lookup.us-east-1.elasticbeanstalk.com/'

//Colors
const {brand, darkLight, primary,darkblue,blue,royalblue} = Colors;
var localStorage = require('localStorage');

// keyboard avoiding view
import KeyboardAvoidingWrapper from '../../components/KeyboardAvoidingWrapper';

const Them_mon_hoc = ({navigation}) => {
    const [message, setMessage] = useState();
    const [ messageType, setMessageType] = useState();

    const handleAdd= (credentials, setSubmitting) =>{
        HandleMessage(null);
        setSubmitting(false);

        const Subject = {
            subjectCode: credentials.subjectCode,
            subjectName: credentials.subjectName,
            teacherCode: credentials.teacherCode
        }
        // ----------------------------------------------------------------------------------------
        // axios get token
        const token = localStorage.getItem("token");
        const USER_TOKEN = token ? "Bearer ".concat(token) : null;
        axios.defaults.headers= {Authorization: USER_TOKEN}; // luu vao header
        const url = 'http://pbl6-point-lookup.us-east-1.elasticbeanstalk.com/api/addSubject';
        
        axios
        .post(url, Subject, { headers: {"Authorization" : USER_TOKEN} })
        .then((response)=> {
            // console.warn(response.data);
            HandleMessage("Thêm thành công")
            })
        .catch((error) => {
            // console.warn(error);
            HandleMessage("Thêm thất bại")})

    }


    const HandleMessage = (message, type= 'FAILED') => {
        setMessage(message);
        setMessageType(type);
    }

    return(
        <KeyboardAvoidingWrapper>
        <>
            <StatusBar style= "darklight" /> 
            <InnerContainer>
                <AvatarContainer resizeMode="cover" source={require('../../assets/img/img3.png')}>
                    <Avatar resizeMode="cover" source={require('../../assets/img/avatar.png')} />
                </AvatarContainer>

                <View style={{paddingTop: 25}} />
                <Formik 
                    initialValues = {{subjectCode: '', subjectName: '', teacherCode: ''}}
                    onSubmit = {(values, {setSubmitting}) => {
                        if(values.subjectCode == '' || values.subjectName == '' ||values.teacherCode == '') {
                            HandleMessage('Vui lòng điền đủ thông tin');
                            setSubmitting(false);
                        }else if(values.password != values.confirmpassword){
                            HandleMessage('Vui lòng nhập đúng thông tin');
                            setSubmitting(false); 
                        }
                        else{
                            handleAdd(values, setSubmitting);
                        }
                    }}
                >
                    {({handleChange, handleBlur, handleSubmit, values,isSubmitting}) => (<StyledFormArea>

                        <MyTextInPut 
                            label="Mã học phần"
                            onChangeText = {handleChange('subjectCode')}
                            onBlur = {handleBlur('subjectCode')}
                            value= {values.subjectCode}
                        />

                        <MyTextInPut 
                            label="Tên môn học"
                            onChangeText = {handleChange('subjectName')}
                            onBlur = {handleBlur('subjectName')}
                            value= {values.subjectName}
                        />
                        
                        <MyTextInPut 
                        label="Mã giáo viên"
                        onChangeText = {handleChange('teacherCode')}
                        onBlur = {handleBlur('teacherCode')}
                        value= {values.teacherCode}
                    />
                        <MsgBox1 type={messageType}>{message}</MsgBox1>

                        <Line />
                        {!isSubmitting &&  <StyledButtonSearch onPress={handleSubmit}>
                            <ButtonText>Thêm học phần</ButtonText>
                        </StyledButtonSearch> }

                        {isSubmitting &&  <StyledButtonSearch disable={true}>
                            <ActivityIndicator size="large" color={primary} />
                        </StyledButtonSearch> }


                        <StyledButtonSearch  style={{paddingBottom: 20}} onPress={() => navigation.dispatch(StackActions.replace('Danh_sach_mon_hoc'))}>
                        <ButtonText>Go Back</ButtonText>
                        </StyledButtonSearch>

                    </StyledFormArea>)}
                </Formik>
            </InnerContainer>
        </>
        </KeyboardAvoidingWrapper>
    );
};

const MyTextInPut = ({label, icon, isPassword, hidePassword, setHidePassword, ...props}) =>{
    return(
        <View style={{paddingTop: 6}}>
            {/* <LeftIcon>
                <Octicons name={icon} size={30} color={brand} />
            </LeftIcon> */}
            <StyledInputLabel>{label}</StyledInputLabel>
            <StyledTextInput_1 {...props} />
            {isPassword && (
                <RightIcon1 onPress={() => setHidePassword(!hidePassword)}>
                    <Ionicons name={hidePassword ? 'md-eye-off' : 'md-eye'}  size={30} color={darkLight} />
                </RightIcon1>
            )}
        </View>
    );
}

export default Them_mon_hoc;