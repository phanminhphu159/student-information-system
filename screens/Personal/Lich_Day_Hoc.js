import React, { Component } from "react";
import { StyleSheet, View, Button, Picker, Alert,  ScrollView, Text, TouchableOpacity } from "react-native";
import { Table, TableWrapper, Row, Cell } from 'react-native-table-component';
import{ Colors} from '../../components/styles'
import Xem_Danh_Sach_SV_Trong_HP from '../../screens/Personal/Xem_Danh_Sach_SV_Trong_HP';


//Colors
const {tertiary} = Colors;



import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
const Stack = createStackNavigator();
const Lich_Day_Hoc = () => {
    return(
        // <NavigationContainer>   
            <Stack.Navigator
                
                screenOptions={{
                    headerStyle: {
                        backgroundColor: 'transparent'
                    },
                    headerTintColor: tertiary,
                    headerTransparent: true,
                    headerTitle: '',
                    headerLeftContainerStyle:{
                        paddingLeft: 20
                    },
                    headerLeft: null,
                }}
                initialRouteName="Lich_Day_Hoc"
            >
                <Stack.Screen name="Xem_Danh_Sach_SV_Trong_HP" component={Xem_Danh_Sach_SV_Trong_HP} />
                <Stack.Screen name="Lich_Day_Hoc" component={Lich_Day_Hoc1} />
                {/* <Stack.Screen options={{ headerTintColor: primary }} name="Welcome" component={Welcome} /> */}
            </Stack.Navigator>
        // </NavigationContainer>
    );
}

 
class Lich_Day_Hoc1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
        tableHead: ['TT','Mã lớp học phần', 'Tên lớp học phần', 'Số TC', 'Số lượng SV', 'Thời khóa biểu', 'Tuần học', 'Xem DS'],
        widthArr: [40, 140, 140, 120, 160, 140, 140, 180],
        PickerSelectedVal : ''
    }
  }


  getSelectedPickerValue=()=>{
    Alert.alert("Selected country is : " +this.state.PickerSelectedVal);
  }

  _alertIndex(index) {
    Alert.alert(`This is row ${index + 1}`);
  }

 
  render() {
    const state = this.state;
    const { navigate } = this.props.navigation;
    const element = (data, index) => (
        <TouchableOpacity onPress={() => {
            this._alertIndex(index);
            this.props.navigation.navigate("Xem_Danh_Sach_SV_Trong_HP",{
              test: index + 1,
            });
        }}>
          <View style={styles.btn}>
            <Text style={styles.btnText}>Xem DS</Text>
          </View>
        </TouchableOpacity>
      );
    const tableData = [];
    for (let i = 0; i < 12; i += 1) {
      const rowData = [];
      for (let j = 0; j < 9; j += 1) {
        rowData.push(`${i}${j}`);
      }
      tableData.push(rowData);
    }

    const state1 = {
      tableHead: ['Mã chương trình', 'Tên chương trình đào tạo', 'Số học kỳ', 'Tổng số tín chỉ yêu cầu', 'Số tín chỉ bắt buộc', 'Số tín chỉ tự chọn', 'Mã ngành'],
      widthArr: [140, 220, 120, 160, 140, 140, 140]
    }
    const rowData1 = ['1021074','Công nghệ Thông tin K2018CLC Đặc thù_HTTT','8','120','113','7','7480201'];
    const tableData1 = [];
    tableData1.push(rowData1);


    return (
      <View style={styles.container}>

        {/* <View style={styles.card}>
        <Text style={{fontSize: 18,textAlign: "left"}}> Khoa : </Text>
        <View style={styles.container_dropdown}>
        <Picker
            style={{ height: 30 }}
            itemStyle={{ backgroundColor: "grey", color: "blue", fontFamily:"Ebrima", fontSize:17 }}
            selectedValue={this.state.PickerSelectedVal}
            onValueChange={(itemValue, itemIndex) => this.setState({PickerSelectedVal: itemValue})} >

            <Picker.Item label="India" value="India" />
            <Picker.Item label="USA" value="USA" />
            <Picker.Item label="China" value="China" />
            <Picker.Item label="Russia" value="Russia" />
            <Picker.Item label="United Kingdom" value="United Kingdom" />
            <Picker.Item label="France" value="France" />   
        </Picker>
        </View>
        </View> 
        <Button s title="Get Selected Picker Value" onPress={ this.getSelectedPickerValue } />*/}

        <View style={{paddingTop: 5}}></View>
        <Text style={{fontSize: 18}}> Thời khoá biểu dạy học </Text>
        <ScrollView horizontal={true}>
            <View style={{height: 530}}> 
            <Table borderStyle={{borderWidth: 1, borderColor: '#C1C0B9'}} style={{paddingTop:5}}>
              <Row data={state.tableHead} widthArr={state.widthArr} style={styles.header} textStyle={styles.text}/>
            </Table>
            <ScrollView style={styles.dataWrapper} >
              <Table 
                borderStyle={{borderWidth: 1, borderColor: '#C1C0B9'}} 
                // style={{height:300, flex: 1}} 
              >
                {   
                  tableData.map((rowData, index) => (
                    <Row
                      key={index}
                      data = {
                          rowData.map((cellData, cellIndex) => (
                            <Cell key={cellIndex} data={cellIndex === 7 ? element(cellData, index) : cellData} textStyle={styles.text} style={{alignItems:"center"}}/>
                          ))
                      }
                      widthArr={state.widthArr}
                      style={[styles.row, index%2 && {backgroundColor: '#F7F6E7'}]}
                      textStyle={styles.text}
                    />
                  ))
                }
              </Table>
            {/* <View style={{paddingBottom: 60}}></View> */}
            </ScrollView>
          </View>
        </ScrollView>
      </View>
    )
  }
}
 

const styles = StyleSheet.create({
  container: { flex: 1, padding: 16, paddingTop: 13, backgroundColor: '#fff' },
  header: { height: 50, backgroundColor: '#537791' },
  text: { textAlign: 'center', fontWeight: '100' },
  dataWrapper: { marginTop: -1 },
  row: { height: 40, backgroundColor: '#E7E6E1' },
  btn: { width: 70, height: 20, backgroundColor: '#78B7BB',  borderRadius: 2 },
  btnText: { textAlign: 'center', color: '#fff' },
  container_dropdown: {
    flex:1,
    borderWidth: 1,
    borderColor: "blue",
    borderRadius: 10,
  },
  card: {
    alignItems:'center',
    flexDirection: 'row',
    paddingBottom: 10
  },
});


export default Lich_Day_Hoc;


// export default HomeActivity;
// export default Chuong_Trinh_Dao_Tao;