import React, { Component,useState,useEffect } from "react";
import { StyleSheet, View, Button, Picker, Alert,  ScrollView, Text, TouchableOpacity } from "react-native";
import { Table, TableWrapper, Row, Cell } from 'react-native-table-component';
import{ Colors,StyledButtonSearch,ButtonText} from '../../components/styles'
import Xem_Danh_Sach_Lop from '../../screens/Search/Xem_Danh_Sach_Lop';

import GetMajor from '../../components/GetMajor';
import GetClassFromMajor from '../../components/GetClassFromMajor';
import GetUser from '../../components/GetUser';
import Them_sinh_vien from '../../screens/Personal/Them_sinh_vien';

import { StackActions } from '@react-navigation/native';

import axios from 'axios';
var localStorage = require('localStorage');
//Colors
const {tertiary,header_color} = Colors;


 


const Xem_Danh_Sach_SV_Trong_HP = (props) => {

  const [selectedVal, setSelectedVal] = useState(null);
  const [tableData, setTableData] = useState([]);
  const [isCalled, setCalled] = useState(false);
  
  const state = {
    tableHead: ['TT', 'Mã sinh viên','Tên sinh viên','Lớp', 'Số điện thoại', 'Địa chỉ email','Điểm bài tập', 'Điểm giữa kì', 'Điểm cuối kì','Điểm thang 10','Điểm chữ', 'Thêm','Sửa'],
    widthArr: [40,180,140, 100,140,180,100,100,100,100,100,100,100],
   }

    const token = localStorage.getItem("token");
    const USER_TOKEN = token ? "Bearer ".concat(token) : null;
    axios.defaults.headers= {Authorization: USER_TOKEN}; 
    const subjectcode = props.route.params.subjectcode;
    const subjectname = props.route.params.subjectname;
    const teachercode = props.route.params.teachercode;
    useEffect(() => {
    if (USER_TOKEN && !isCalled) {
        // setCalled(true);
      const url = 'http://pbl6-point-lookup.us-east-1.elasticbeanstalk.com/api/listStudentOfSubject?subjectCode='.concat(subjectcode); 
      axios
            .get(url)
            .then((resp)=> {
              const MyArr = resp.data;
              if(MyArr.data == null){

              }else{
              var Table = MyArr.data.map(record=>([record.person.fullName, record.person.studentCode,record.classCode, record.person.phone, record.person.email, JSON.stringify(record.scores.map(item=>item.assignmentScore)[0]),JSON.stringify(record.scores.map(item=>item.midtermScore)[0]),JSON.stringify(record.scores.map(item=>item.finalScore)[0]),numScore(JSON.stringify(record.scores.map(item=>item.assignmentScore)[0]), JSON.stringify(record.scores.map(item=>item.midtermScore)[0]), JSON.stringify(record.scores.map(item=>item.finalScore)[0])),averageLetScore(numScore(JSON.stringify(record.scores.map(item=>item.assignmentScore)[0]), JSON.stringify(record.scores.map(item=>item.midtermScore)[0]), JSON.stringify(record.scores.map(item=>item.finalScore)[0])))]));
              

              for (let i = 0; i < Table.length; i += 1) {
                Table[i].unshift(i + 1);
              }for (let i = 0; i < Table.length; i += 1) {
                Table[i].push('');
                Table[i].push('');
              }


              setTableData(Table);}
        })
        .catch((error) => {
            // console.warn(error);
          })
        }
        }, []);

  const AddStudent = () => {
    props.navigation.navigate("Them_sinh_vien",{
      subjectcode: subjectcode,
      subjectname: subjectname,
      teachercode: teachercode,
    });
    };

  const _alertIndex=(index) => {
    Alert.alert(`This is row ${index + 1}`);
  }

 
    const Edit = (data, index) => (
        <TouchableOpacity onPress={() => {
            props.navigation.dispatch(StackActions.replace('Sua_diem',{
              subjectcode: subjectcode,
              subjectname: subjectname,
              teachercode: teachercode,
              studentcode: tableData[index][2],
            }))
        }}>
          <View style={styles.btn}>
            <Text style={styles.btnText}>Sửa</Text>
          </View>
        </TouchableOpacity>
      );


      
    const Add = (data, index) => (
      <TouchableOpacity onPress={() => {
          props.navigation.dispatch(StackActions.replace('Them_diem',{
            subjectcode: subjectcode,
            subjectname: subjectname,
            teachercode: teachercode,
            studentcode: tableData[index][2],
          }))
      }}>
        <View style={styles.btn}>
          <Text style={styles.btnText}>Thêm</Text>
        </View>
      </TouchableOpacity>
    );
    const numScore = (AssignmentScore,MidtermScore,FinalScore) => {
      
      
      if (AssignmentScore == undefined) {
        return null;
      } else{
        const ave = AssignmentScore*0.2 + MidtermScore*0.3 + FinalScore*0.5;
        return Math.round(ave*100)/100;
      }
    
    };
  
    const averageLetScore = (numScore) => {

      if (numScore == null) {
        return "";
      } 
      else if (numScore >= 9) {
        return "A+";
      } else if (numScore >= 8.5) {
        return "A";
      } else if (numScore >= 7.5) {
        return "B+";
      } else if (numScore >= 7) {
        return "B";
      } else if (numScore >= 6.5) {
        return "C+";
      } else if (numScore >= 5.5) {
        return "C";
      } else if (numScore >= 5) {
        return "D+";
      } else if (numScore >= 4) {
        return "D";
      }else {
        return "F";
      }
    };

    return (
      <View style={styles.container}>
        <View style={{paddingTop: 10}}></View>
        <Text style={{fontSize: 18}}> Danh sách sinh viên trong học phần </Text>
        <View style={{paddingTop: 5}}></View>
        <ScrollView horizontal={true}>
            <View style={{height: 385, paddingTop: 5}}> 
            <Table borderStyle={{borderWidth: 1, borderColor: '#C1C0B9'}} style={{paddingTop:5}}>
              <Row data={state.tableHead} widthArr={state.widthArr} style={styles.header} textStyle={styles.text}/>
            </Table>
            <ScrollView style={styles.dataWrapper} >
              <Table 
                borderStyle={{borderWidth: 1, borderColor: '#C1C0B9'}} 
                // style={{height:300, flex: 1}} 
              >
                {   
                  tableData.map((rowData, index) => (
                    <Row
                      key={index}
                      data = {
                          rowData.map((cellData, cellIndex) => (
                            <Cell key={cellIndex} data={cellIndex === 12 ? Edit(cellData, index): cellIndex === 11 ? Add(cellData, index) : cellData} textStyle={styles.text} style={{alignItems:"center"}}/>
                          ))
                      }
                      widthArr={state.widthArr}
                      style={[styles.row, index%2 && {backgroundColor: '#F7F6E7'}]}
                      textStyle={styles.text}
                    />
                  ))
                }
              </Table>
            </ScrollView>
          </View>
        </ScrollView>
        <View style={{paddingBottom: 70}}>
        <StyledButtonSearch style={{paddingBottom: 20}} onPress={AddStudent}>
        <ButtonText>Thêm sinh viên</ButtonText>
        </StyledButtonSearch>

        <StyledButtonSearch  style={{paddingBottom: 20}} onPress={() => props.navigation.dispatch(StackActions.replace('Danh_sach_mon_hoc'))}>
        <ButtonText>Trở về</ButtonText>
        </StyledButtonSearch>
        </View>
      </View>
    )
}
 

const styles = StyleSheet.create({
  container: { flex: 1, padding: 16, paddingTop: 13, backgroundColor: '#fff' },
  header: { height: 50, backgroundColor: header_color },
  text: { textAlign: 'center', fontWeight: '100' },
  dataWrapper: { marginTop: -1 },
  row: { height: 40, backgroundColor: '#E7E6E1' },
  btn: { width: 70, height: 20, backgroundColor: '#78B7BB',  borderRadius: 2 },
  btnText: { textAlign: 'center', color: '#fff' },
  container_dropdown: {
    flex:1,
    borderWidth: 1,
    borderColor: "blue",
    borderRadius: 10,
  },
  card: {
    alignItems:'center',
    flexDirection: 'row',
    paddingBottom: 10
  },
});


export default Xem_Danh_Sach_SV_Trong_HP;

