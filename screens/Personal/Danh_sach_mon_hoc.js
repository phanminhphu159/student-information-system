import React, { Component,useState,useEffect } from "react";
import { StyleSheet, View, Button, Picker, Alert,  ScrollView, Text, TouchableOpacity } from "react-native";
import { Table, TableWrapper, Row, Cell } from 'react-native-table-component';
import{ Colors,StyledButtonSearch,ButtonText} from '../../components/styles'
import Xem_Danh_Sach_Lop from '../../screens/Search/Xem_Danh_Sach_Lop';

import GetMajor from '../../components/GetMajor';
import GetClassFromMajor from '../../components/GetClassFromMajor';
import GetUser from '../../components/GetUser';
import Xem_Danh_Sach_SV_Trong_HP from '../../screens/Personal/Xem_Danh_Sach_SV_Trong_HP';
import Them_mon_hoc from '../../screens/Personal/Them_mon_hoc';
import Them_sinh_vien from '../../screens/Personal/Them_sinh_vien';
import Sua_diem from '../../screens/Personal/Sua_diem';
import Them_diem from '../../screens/Personal/Them_diem';

import { StackActions } from '@react-navigation/native';

import axios from 'axios';
var localStorage = require('localStorage');
//Colors
const {tertiary,header_color} = Colors;

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
const Stack = createStackNavigator();
const Danh_sach_mon_hoc = () => {
    return(
        // <NavigationContainer>   
            <Stack.Navigator
                
                screenOptions={{
                    headerStyle: {
                        backgroundColor: 'transparent'
                    },
                    headerTintColor: tertiary,
                    headerTransparent: true,
                    headerTitle: '',
                    headerLeftContainerStyle:{
                        paddingLeft: 20
                    },
                    headerLeft: null,
                }}
                initialRouteName="Danh_sach_mon_hoc"
            >
                <Stack.Screen name="Xem_Danh_Sach_SV_Trong_HP" component={Xem_Danh_Sach_SV_Trong_HP} />
                <Stack.Screen name="Danh_sach_mon_hoc" component={Table_Search} />
                <Stack.Screen name="Them_mon_hoc" component={Them_mon_hoc} />
                <Stack.Screen name="Them_sinh_vien" component={Them_sinh_vien} />
                <Stack.Screen name="Sua_diem" component={Sua_diem} />
                <Stack.Screen name="Them_diem" component={Them_diem} />
                {/* <Stack.Screen options={{ headerTintColor: primary }} name="Welcome" component={Welcome} /> */}
            </Stack.Navigator>
        // </NavigationContainer>
    );
}

 


const Table_Search = ({navigation},props) => {
  const [selectedVal, setSelectedVal] = useState(null);
  const [tableData, setTableData] = useState([]);
  const [isCalled, setCalled] = useState(false);
  
  const state = {
    tableHead: ['TT', 'Mã học phần','Tên môn học', 'Giáo viên', 'Xem danh sách'],
    widthArr: [40,140,180, 140,100],
   }

    const token = localStorage.getItem("token");
    const USER_TOKEN = token ? "Bearer ".concat(token) : null;
    axios.defaults.headers= {Authorization: USER_TOKEN}; 
    useEffect(() => {
    if (USER_TOKEN && !isCalled) {
        setCalled(true);
      const url = 'http://pbl6-point-lookup.us-east-1.elasticbeanstalk.com/api/findAllSubject'; 
      axios
            .get(url)
            .then((resp)=> {
              const MyArr = resp.data;
            //   console.warn(MyArr);

              var Table = MyArr.data.map(record=>([record.subjectCode,record.subjectName, record.teacherCode]));
              for (let i = 0; i < Table.length; i += 1) {
                Table[i].unshift(i + 1);
              }for (let i = 0; i < Table.length; i += 1) {
                Table[i].push('');
              }
              setTableData(Table);
        })
        .catch((error) => {
            // console.warn(error);
          })
        }
        }, []);

  const AddSubject = () => {
    navigation.navigate("Them_mon_hoc");
    };

  const _alertIndex=(index) => {
    Alert.alert(`This is row ${index + 1}`);
  }

 
    const element = (data, index) => (
        <TouchableOpacity onPress={() => {
            navigation.dispatch(StackActions.replace('Xem_Danh_Sach_SV_Trong_HP',{
                subjectcode: tableData[index][1],
                subjectname: tableData[index][2],
                teachercode: tableData[index][3],})
            )}
            }>
          <View style={styles.btn}>
            <Text style={styles.btnText}>Xem DS</Text>
          </View>
        </TouchableOpacity>
      );
      

    return (
      <View style={styles.container}>
        <View style={{paddingTop: 10}}></View>
        <Text style={{fontSize: 18}}> Danh sách môn học </Text>
        <View style={{paddingTop: 5}}></View>
        <ScrollView horizontal={true}>
            <View style={{height: 424, paddingTop: 5}}> 
            <Table borderStyle={{borderWidth: 1, borderColor: '#C1C0B9'}} style={{paddingTop:5}}>
              <Row data={state.tableHead} widthArr={state.widthArr} style={styles.header} textStyle={styles.text}/>
            </Table>
            <ScrollView style={styles.dataWrapper} >
              <Table 
                borderStyle={{borderWidth: 1, borderColor: '#C1C0B9'}} 
                // style={{height:300, flex: 1}} 
              >
                {   
                  tableData.map((rowData, index) => (
                    <Row
                      key={index}
                      data = {
                          rowData.map((cellData, cellIndex) => (
                            <Cell key={cellIndex} data={cellIndex === 4 ? element(cellData, index) : cellData} textStyle={styles.text} style={{alignItems:"center"}}/>
                          ))
                      }
                      widthArr={state.widthArr}
                      style={[styles.row, index%2 && {backgroundColor: '#F7F6E7'}]}
                      textStyle={styles.text}
                    />
                  ))
                }
              </Table>
            </ScrollView>
          </View>
        </ScrollView>
        <View style={{paddingBottom: 70}}>
        <StyledButtonSearch style={{paddingBottom: 20}} onPress={AddSubject}>
        <ButtonText>Thêm môn học</ButtonText>
        </StyledButtonSearch>
        </View>
      </View>
    )
}
 

const styles = StyleSheet.create({
  container: { flex: 1, padding: 16, paddingTop: 13, backgroundColor: '#fff' },
  header: { height: 50, backgroundColor: header_color },
  text: { textAlign: 'center', fontWeight: '100' },
  dataWrapper: { marginTop: -1 },
  row: { height: 40, backgroundColor: '#E7E6E1' },
  btn: { width: 70, height: 20, backgroundColor: '#78B7BB',  borderRadius: 2 },
  btnText: { textAlign: 'center', color: '#fff' },
  container_dropdown: {
    flex:1,
    borderWidth: 1,
    borderColor: "blue",
    borderRadius: 10,
  },
  card: {
    alignItems:'center',
    flexDirection: 'row',
    paddingBottom: 10
  },
});


export default Danh_sach_mon_hoc;

