import React, { Component,useState } from "react";
import { StyleSheet, View, Button, Picker, Alert,  ScrollView, Text, TouchableOpacity } from "react-native";
import { Table, TableWrapper, Row, Cell } from 'react-native-table-component';
import{ Colors,StyledButtonSearch,ButtonText} from '../../components/styles'
import Xem_Danh_Sach_Lop from '../../screens/Search/Xem_Danh_Sach_Lop';

import GetMajor from '../../components/GetMajor';
import GetClassFromMajor from '../../components/GetClassFromMajor';
import GetUser from '../../components/GetUser';

import axios from 'axios';
var localStorage = require('localStorage');
//Colors
const {tertiary,header_color} = Colors;

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
const Stack = createStackNavigator();
const Ket_Qua_Hoc_Tap = () => {
    return(
        // <NavigationContainer>   
            <Stack.Navigator
                
                screenOptions={{
                    headerStyle: {
                        backgroundColor: 'transparent'
                    },
                    headerTintColor: tertiary,
                    headerTransparent: true,
                    headerTitle: '',
                    headerLeftContainerStyle:{
                        paddingLeft: 20
                    },
                    headerLeft: null,
                }}
                initialRouteName="Chuong_Trinh_Dao_Tao"
            >
                {/* <Stack.Screen name="Xem_Danh_Sach_Lop" component={Xem_Danh_Sach_Lop} /> */}
                <Stack.Screen name="Ket_Qua_Hoc_Tap" component={Table_Search} />
                {/* <Stack.Screen options={{ headerTintColor: primary }} name="Welcome" component={Welcome} /> */}
            </Stack.Navigator>
        // </NavigationContainer>
    );
}

 


const Table_Search = ({navigation}) => {
  const [selectedVal, setSelectedVal] = useState(null);
  const [tableData, setTableData] = useState([]);
  const [isCalled, setCalled] = useState(false);
  const state = {
    tableHead: ['TT', 'Mã học phần','Tên môn học', 'Giáo viên', 'Điểm bài tập','Điểm giữa kì','Điểm cuối kì','Điểm thang 10','Điểm chữ'],
    widthArr: [40,140,180, 140,100,100,100,100, 100],
   }

  // User:
  const MyUser = GetUser();
  const token = localStorage.getItem("token");
  const USER_TOKEN = token ? "Bearer ".concat(token) : null;
  axios.defaults.headers= {Authorization: USER_TOKEN};
  if (USER_TOKEN && !isCalled && MyUser != null) {
    setCalled(true);
    var student_code = MyUser.data.studentCode;
    // console.warn(student_code); 
      const url = 'http://pbl6-point-lookup.us-east-1.elasticbeanstalk.com/api/findAllScoreByStudentCode?studentCode='.concat(student_code); 
      axios
            .get(url)
            .then((resp)=> {
              const MyArr = resp.data;
              var Table = MyArr.data.map(record=>([record.subjects.subjectCode, record.subjects.subjectName, record.subjects.teacherCode, record.assignmentScore, record.midtermScore, record.finalScore,numScore(record.assignmentScore, record.midtermScore, record.finalScore),averageLetScore(numScore(record.assignmentScore, record.midtermScore, record.finalScore))]));
              for (let i = 0; i < Table.length; i += 1) {
                Table[i].unshift(i + 1);
              }

              setTableData(Table);
        })
        .catch((error) => {
            // console.warn(error);
          })
  }

  // const getTable=(param)=>{
  //   console.warn("yUser.data.studentCode");
  //   const token = localStorage.getItem("token");
  //   const USER_TOKEN = token ? "Bearer ".concat(token) : null;
  //   axios.defaults.headers= {Authorization: USER_TOKEN}; 
  //     const url = 'http://pbl6-point-lookup.us-east-1.elasticbeanstalk.com/api/listSubjectOfStudent?studentCode=student1'.concat(param); 
  //     axios
  //           .get(url)
  //           .then((resp)=> {
  //             const MyArr = resp.data;
  //             // console.warn(MyArr);
  //             var Table = MyArr.data.map(record=>([record.className, record.classCode]));
  //             // for (let i = 0; i < Table.length; i += 1) {
  //             //   Table[i].push("");
  //             // }
  //             for (let i = 0; i < Table.length; i += 1) {
  //               Table[i].unshift(i + 1);
  //             }

  //             setTableData(Table);
  //       })
  //       .catch((error) => {
  //           console.warn(error);})
  // }

  const _alertIndex=(index) => {
    Alert.alert(`This is row ${index + 1}`);
  }

  const numScore = (AssignmentScore,MidtermScore,FinalScore) => {
    if (AssignmentScore == undefined) {
      return null;
    } else{
      const ave = AssignmentScore*0.2 + MidtermScore*0.3 + FinalScore*0.5;
      return Math.round(ave*100)/100;
    }
  };

  const averageLetScore = (numScore) => {
    if (numScore >= 9) {
      return "A+";
    } else if (numScore >= 8.5) {
      return "A";
    } else if (numScore >= 7.5) {
      return "B+";
    } else if (numScore >= 7) {
      return "B";
    } else if (numScore >= 6.5) {
      return "C+";
    } else if (numScore >= 5.5) {
      return "C";
    } else if (numScore >= 5) {
      return "D+";
    } else if (numScore >= 4) {
      return "D";
    } else {
      return "F";
    }
  };

 
    // const element = (data, index) => (
    //     <TouchableOpacity onPress={() => {
    //         _alertIndex(index);
    //         navigation.navigate("Xem_Danh_Sach_Lop",{
    //           test: index + 1,
    //         );
    //     }}>
    //       <View style={styles.btn}>
    //         <Text style={styles.btnText}>Xem DS</Text>
    //       </View>
    //     </TouchableOpacity>
    //   );

    return (
      <View style={styles.container}>
        <View style={{paddingTop: 10}}></View>
        <Text style={{fontSize: 18}}> Kết quả học tập </Text>
        <ScrollView horizontal={true}>
            <View style={{height: 417, paddingTop: 5}}> 
            <Table borderStyle={{borderWidth: 1, borderColor: '#C1C0B9'}} style={{paddingTop:5}}>
              <Row data={state.tableHead} widthArr={state.widthArr} style={styles.header} textStyle={styles.text}/>
            </Table>
            <ScrollView style={styles.dataWrapper} >
              <Table 
                borderStyle={{borderWidth: 1, borderColor: '#C1C0B9'}} 
                // style={{height:300, flex: 1}} 
              >
                {   
                  tableData.map((rowData, index) => (
                    <Row
                      key={index}
                      data = {
                          rowData.map((cellData, cellIndex) => (
                            <Cell key={cellIndex} data={cellData} textStyle={styles.text} style={{alignItems:"center"}}/>
                          ))
                      }
                      widthArr={state.widthArr}
                      style={[styles.row, index%2 && {backgroundColor: '#F7F6E7'}]}
                      textStyle={styles.text}
                    />
                  ))
                }
              </Table>
            {/* <View style={{paddingBottom: 60}}></View> */}
            </ScrollView>
          </View>
        </ScrollView>
      </View>
    )
}
 

const styles = StyleSheet.create({
  container: { flex: 1, padding: 16, paddingTop: 13, backgroundColor: '#fff' },
  header: { height: 50, backgroundColor: header_color },
  text: { textAlign: 'center', fontWeight: '100' },
  dataWrapper: { marginTop: -1 },
  row: { height: 40, backgroundColor: '#E7E6E1' },
  btn: { width: 70, height: 20, backgroundColor: '#78B7BB',  borderRadius: 2 },
  btnText: { textAlign: 'center', color: '#fff' },
  container_dropdown: {
    flex:1,
    borderWidth: 1,
    borderColor: "blue",
    borderRadius: 10,
  },
  card: {
    alignItems:'center',
    flexDirection: 'row',
    paddingBottom: 10
  },
});


export default Ket_Qua_Hoc_Tap;

