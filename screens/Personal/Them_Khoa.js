
import React, {useState,useEffect} from 'react';
import { StatusBar } from 'expo-status-bar';

// create components
import {Formik, formik} from 'formik';
import {Octicons, Ionicons, Fontisto} from '@expo/vector-icons';
import {View, ScrollView,ActivityIndicator} from 'react-native';
import{ StyledContainer, RightIcon1, InnerContainer, PageLogo, PageTitle, SubTitle, StyledFormArea, LeftIcon, StyledInputLabel, StyledInputLabel_1 , StyledTextInput, StyledTextInput_1, RightIcon, 
    StyledButton, ButtonText, Colors, MsgBox,MsgBox1, Line, ExtraView, ExtraText, TextLink, TextLinkContent, WelcomeContainer, TTSV_Container, WelcomeImage, Avatar, AvatarContainer,StyledButtonSearch} from '../../components/styles';


import { StackActions } from '@react-navigation/native';

// API
import axios from 'axios';
axios.defaults.baseURL = 'http://pbl6-point-lookup.us-east-1.elasticbeanstalk.com/'

//Colors
const {brand, darkLight, primary,darkblue,blue,royalblue} = Colors;
var localStorage = require('localStorage');

// keyboard avoiding view
import KeyboardAvoidingWrapper from '../../components/KeyboardAvoidingWrapper';

const Them_Khoa = (props) => {
    const [message, setMessage] = useState();
    const [ messageType, setMessageType] = useState();
    // const majorcode = props.route.params.majorcode;

    const handleAdd= (credentials, setSubmitting) =>{
        HandleMessage(null);
        setSubmitting(false);

        

        const Scores = {
            majorCode: credentials.majorcode,
            majorName: credentials.majorname
          }
        // ----------------------------------------------------------------------------------------
        // axios get token
        const token = localStorage.getItem("token");
        const USER_TOKEN = token ? "Bearer ".concat(token) : null;
        axios.defaults.headers= {Authorization: USER_TOKEN}; // luu vao header
        const url = 'http://pbl6-point-lookup.us-east-1.elasticbeanstalk.com/api/addMajor';
        
        axios
        .post(url, Scores, { headers: {"Authorization" : USER_TOKEN} })
        .then((response)=> {
            // console.warn(response.data);
            HandleMessage("Thêm thành công")
            })
        .catch((error) => {
            // console.warn(error);
            HandleMessage("Thêm thất bại")})

    }


    const HandleMessage = (message, type= 'FAILED') => {
        setMessage(message);
        setMessageType(type);
    }

    return(
        <KeyboardAvoidingWrapper>
        <>
            <StatusBar style= "darklight" /> 
            <InnerContainer>
                <AvatarContainer resizeMode="cover" source={require('../../assets/img/img3.png')}>
                <Avatar resizeMode="cover" source={require('../../assets/img/avatar.png')} />
                </AvatarContainer>

                <View style={{paddingTop: 15}} />
                <Formik 
                    initialValues = {{majorcode: '',majorname: ''}}
                    onSubmit = {(values, {setSubmitting}) => {
                        if( values.majorcode == ''|| values.majorname == '') {
                            HandleMessage('Vui lòng điền đủ thông tin');
                            setSubmitting(false);
                        }else if(values.password != values.confirmpassword){
                            HandleMessage('Vui lòng nhập đúng thông tin');
                            setSubmitting(false); 
                        }
                        else{
                            handleAdd(values, setSubmitting);
                        }
                    }}
                >
                    {({handleChange, handleBlur, handleSubmit, values,isSubmitting}) => (<StyledFormArea>

                        <View style={{paddingTop: 25}} />

                        <MyTextInPut 
                            label="Tên khoa"
                            onChangeText = {handleChange('majorname')}
                            onBlur = {handleBlur('majorname')}
                            value= {values.majorname}
                        />


                        <MyTextInPut 
                            label="Mã khoa"
                            onChangeText = {handleChange('majorcode')}
                            onBlur = {handleBlur('majorcode')}
                            value= {values.majorcode}
                        />
                        <View style={{paddingTop: 20}} />
                        
                        <MsgBox1 type={messageType}>{message}</MsgBox1>

                        <View style={{paddingTop: 20}} />
                        <Line />
                        {!isSubmitting &&  <StyledButtonSearch onPress={handleSubmit}>
                            <ButtonText>Thêm khoa</ButtonText>
                        </StyledButtonSearch> }

                        {isSubmitting &&  <StyledButtonSearch disable={true}>
                            <ActivityIndicator size="large" color={primary} />
                        </StyledButtonSearch> }


                        <StyledButtonSearch  style={{paddingBottom: 20}} onPress={() => {
                            props.navigation.dispatch(StackActions.replace('Quan_Ly_Khoa')
                            )}
                        }>
                        <ButtonText>Trở về</ButtonText>
                        </StyledButtonSearch>

                    </StyledFormArea>)}
                </Formik>
            </InnerContainer>
        </>
        </KeyboardAvoidingWrapper>
    );
};

const MyTextInPut = ({label, icon, isPassword, hidePassword, setHidePassword, ...props}) =>{
    return(
        <View style={{paddingTop: 6}}>
            {/* <LeftIcon>
                <Octicons name={icon} size={30} color={brand} />
            </LeftIcon> */}
            <StyledInputLabel>{label}</StyledInputLabel>
            <StyledTextInput_1 {...props} />
            {isPassword && (
                <RightIcon1 onPress={() => setHidePassword(!hidePassword)}>
                    <Ionicons name={hidePassword ? 'md-eye-off' : 'md-eye'}  size={30} color={darkLight} />
                </RightIcon1>
            )}
        </View>
    );
}

export default Them_Khoa;