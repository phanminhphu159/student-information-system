
import React, {useState} from 'react';
import { StatusBar } from 'expo-status-bar';

// create components
import {Formik, formik} from 'formik';
import {Octicons, Ionicons, Fontisto} from '@expo/vector-icons';
import {View, ScrollView,ActivityIndicator} from 'react-native';
import{ StyledContainer, RightIcon1, InnerContainer, PageLogo, PageTitle, SubTitle, StyledFormArea, LeftIcon, StyledInputLabel, StyledInputLabel_1 , StyledTextInput, StyledTextInput_1, RightIcon, 
    StyledButton, ButtonText, Colors, MsgBox, Line, ExtraView, ExtraText, TextLink, TextLinkContent, WelcomeContainer, TTSV_Container, WelcomeImage, Avatar, AvatarContainer} from '../../components/styles';

import GetUser from '../../components/GetUser';
    
// API
import axios from 'axios';
axios.defaults.baseURL = 'http://pbl6-point-lookup.us-east-1.elasticbeanstalk.com/'
// axios.defaults.baseURL = 'http://192.168.1.105:8082/'

//Colors
const {brand, darkLight, primary,darkblue,blue,royalblue} = Colors;
var localStorage = require('localStorage');

// keyboard avoiding view
import KeyboardAvoidingWrapper from '../../components/KeyboardAvoidingWrapper';

const Doi_Mat_Khau = ({navigation}) => {
    const [hidePassword , setHidePassword] = useState(true);
    const [hidePassword2 , setHidePassword2] = useState(true);
    const [hidePassword3 , setHidePassword3] = useState(true);
    const [message, setMessage] = useState();
    const [ messageType, setMessageType] = useState();
    const MyUser = GetUser();

    const handleUpdate= (credentials, setSubmitting) =>{
        HandleMessage(null);
        setSubmitting(false);

        const User_Update = {
            password: credentials.password,
            userName: MyUser.data.userName,
        }
        // ----------------------------------------------------------------------------------------
        // axios get token
        const token = localStorage.getItem("token");
        const USER_TOKEN = token ? "Bearer ".concat(token) : null;
        axios.defaults.headers= {Authorization: USER_TOKEN}; // luu vao header


        const url = 'http://pbl6-point-lookup.us-east-1.elasticbeanstalk.com/api/updatePerson';
        axios
        .put(url, User_Update, { headers: {"Authorization" : USER_TOKEN} })
        .then((response)=> {
            HandleMessage("Chỉnh sửa thành công")

            })
        .catch((error) => {
            // console.warn(error);
            HandleMessage("Sai cú pháp")})

    }


    const HandleMessage = (message, type= 'FAILED') => {
        setMessage(message);
        setMessageType(type);
    }

    return(
        <KeyboardAvoidingWrapper>
        <>
            <StatusBar style= "darklight" /> 
            <InnerContainer>
                <AvatarContainer resizeMode="cover" source={require('../../assets/img/img3.png')}>
                    <Avatar resizeMode="cover" source={require('../../assets/img/avatar.png')} />
                </AvatarContainer>

                <View style={{paddingTop: 25}} />
                <Formik 
                    initialValues = {{oldpassword: '', password: '', confirmpassword: ''}}
                    onSubmit = {(values, {setSubmitting}) => {
                        if(values.username == '' || values.password == '' ||values.confirmpassword == '') {
                            HandleMessage('Vui lòng điền đủ thông tin');
                            setSubmitting(false); // nhap tu bao nhieu ki tu tro len
                        }else if(values.password != values.confirmpassword){
                            HandleMessage('Vui lòng nhập đúng thông tin');
                            setSubmitting(false); // nhap tu bao nhieu ki tu tro len
                        }
                        else{
                            handleUpdate(values, setSubmitting);
                        }
                    }}
                >
                    {({handleChange, handleBlur, handleSubmit, values,isSubmitting}) => (<StyledFormArea>

                        <MyTextInPut
                            label="Mật khẩu cũ"
                            placeholder= "* * * * * * * * * *"
                            hidePassword={hidePassword}
                            setHidePassword={setHidePassword}
                            placeholderTextColor= {darkLight}
                            secureTextEntry={hidePassword}
                            isPassword={true}
                            onChangeText = {handleChange('oldpassword')}
                            onBlur = {handleBlur('oldpassword')}
                            value={values.oldpassword}
                        />

                        <MyTextInPut
                            label="Mật khẩu mới"
                            placeholder= "* * * * * * * * * *"
                            placeholderTextColor= {darkLight}
                            onChangeText = {handleChange('password')}
                            onBlur = {handleBlur('password')}
                            value={values.password}
                            secureTextEntry={hidePassword2}
                            isPassword={true}
                            hidePassword={hidePassword2}
                            setHidePassword={setHidePassword2}
                        />

                        <MyTextInPut
                            label="Gõ lại mật khẩu mới"
                            placeholder= "* * * * * * * * * *"
                            placeholderTextColor= {darkLight}
                            onChangeText = {handleChange('confirmpassword')}
                            onBlur = {handleBlur('confirmpassword')}
                            value={values.confirmpassword}
                            secureTextEntry={hidePassword3}
                            isPassword={true}
                            hidePassword={hidePassword3}
                            setHidePassword={setHidePassword3}
                        />
                        <View style={{paddingTop: 5}}/>
                        <MsgBox type={messageType}>{message}</MsgBox>

                        <Line />
                        {!isSubmitting &&  <StyledButton onPress={handleSubmit}>
                            <ButtonText>Đổi mật khẩu</ButtonText>
                        </StyledButton> }

                        {isSubmitting &&  <StyledButton disable={true}>
                            <ActivityIndicator size="large" color={primary} />
                        </StyledButton> }

                    </StyledFormArea>)}
                </Formik>
            </InnerContainer>
        </>
        </KeyboardAvoidingWrapper>
    );
};

const MyTextInPut = ({label, icon, isPassword, hidePassword, setHidePassword, ...props}) =>{
    return(
        <View style={{paddingTop: 6}}>
            {/* <LeftIcon>
                <Octicons name={icon} size={30} color={brand} />
            </LeftIcon> */}
            <StyledInputLabel>{label}</StyledInputLabel>
            <StyledTextInput_1 {...props} />
            {isPassword && (
                <RightIcon1 onPress={() => setHidePassword(!hidePassword)}>
                    <Ionicons name={hidePassword ? 'md-eye-off' : 'md-eye'}  size={30} color={darkLight} />
                </RightIcon1>
            )}
        </View>
    );
}

export default Doi_Mat_Khau;