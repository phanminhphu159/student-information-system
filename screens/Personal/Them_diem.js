
import React, {useState,useEffect} from 'react';
import { StatusBar } from 'expo-status-bar';

// create components
import {Formik, formik} from 'formik';
import {Octicons, Ionicons, Fontisto} from '@expo/vector-icons';
import {View, ScrollView,ActivityIndicator} from 'react-native';
import{ StyledContainer, RightIcon1, InnerContainer, PageLogo, PageTitle, SubTitle, StyledFormArea, LeftIcon, StyledInputLabel, StyledInputLabel_1 , StyledTextInput, StyledTextInput_1, RightIcon, 
    StyledButton, ButtonText, Colors, MsgBox,MsgBox1, Line, ExtraView, ExtraText, TextLink, TextLinkContent, WelcomeContainer, TTSV_Container, WelcomeImage, Avatar, AvatarContainer,StyledButtonSearch} from '../../components/styles';


import { StackActions } from '@react-navigation/native';

// API
import axios from 'axios';
axios.defaults.baseURL = 'http://pbl6-point-lookup.us-east-1.elasticbeanstalk.com/'

//Colors
const {brand, darkLight, primary,darkblue,blue,royalblue} = Colors;
var localStorage = require('localStorage');

// keyboard avoiding view
import KeyboardAvoidingWrapper from '../../components/KeyboardAvoidingWrapper';

const Them_diem = (props) => {
    const [message, setMessage] = useState();
    const [ messageType, setMessageType] = useState();
    const subjectcode = props.route.params.subjectcode;
    const subjectname = props.route.params.subjectname;
    const teachercode = props.route.params.teachercode;
    const studentcode = props.route.params.studentcode;

    const handleAdd= (credentials, setSubmitting) =>{
        HandleMessage(null);
        setSubmitting(false);

        

        const Scores = {
            assignmentScore: credentials.assignmentscore,
            finalScore: credentials.finalscore,
            hardWorkScore: 10,
            midtermScore: credentials.midtermscore,
            subjects: {
              students: [],
              subjectCode: subjectcode,
              subjectName: subjectname,
              teacherCode: teachercode
            }
          }
        // ----------------------------------------------------------------------------------------
        // axios get token
        const token = localStorage.getItem("token");
        const USER_TOKEN = token ? "Bearer ".concat(token) : null;
        axios.defaults.headers= {Authorization: USER_TOKEN}; // luu vao header
        const url = 'http://pbl6-point-lookup.us-east-1.elasticbeanstalk.com/api/addScore?studentCode='.concat(studentcode).concat('&subjectCode=').concat(subjectcode);
        
        axios
        .post(url, Scores, { headers: {"Authorization" : USER_TOKEN} })
        .then((response)=> {
            // console.warn(response.data);
            HandleMessage("Thêm thành công")
            })
        .catch((error) => {
            // console.warn(error);
            HandleMessage("Thêm thất bại")})

    }


    const HandleMessage = (message, type= 'FAILED') => {
        setMessage(message);
        setMessageType(type);
    }

    return(
        <KeyboardAvoidingWrapper>
        <>
            <StatusBar style= "darklight" /> 
            <InnerContainer>
                <AvatarContainer resizeMode="cover" source={require('../../assets/img/img3.png')}>
                <Avatar resizeMode="cover" source={require('../../assets/img/avatar.png')} />
                </AvatarContainer>

                <View style={{paddingTop: 15}} />
                <Formik 
                    initialValues = {{assignmentscore: '',midtermscore: '', finalscore: ''}}
                    onSubmit = {(values, {setSubmitting}) => {
                        if( values.assignmentscore == ''|| values.midtermscore == '' || values.finalscore == '') {
                            HandleMessage('Vui lòng điền đủ thông tin');
                            setSubmitting(false);
                        }else if(values.password != values.confirmpassword){
                            HandleMessage('Vui lòng nhập đúng thông tin');
                            setSubmitting(false); 
                        }
                        else{
                            handleAdd(values, setSubmitting);
                        }
                    }}
                >
                    {({handleChange, handleBlur, handleSubmit, values,isSubmitting}) => (<StyledFormArea>

                        <MyTextInPut 
                            label="Điểm  bài tập"
                            onChangeText = {handleChange('assignmentscore')}
                            onBlur = {handleBlur('assignmentscore')}
                            value= {values.assignmentscore}
                        />
                        <View style={{paddingTop: 10}} />

                        <MyTextInPut 
                            label="Điểm giữa kì"
                            onChangeText = {handleChange('midtermscore')}
                            onBlur = {handleBlur('midtermscore')}
                            value= {values.midtermscore}
                        />

                        <MyTextInPut 
                            label="Điểm cuối kì"
                            onChangeText = {handleChange('finalscore')}
                            onBlur = {handleBlur('finalscore')}
                            value= {values.finalscore}
                        />
                        
                        <MsgBox1 type={messageType}>{message}</MsgBox1>

                        <Line />
                        {!isSubmitting &&  <StyledButtonSearch onPress={handleSubmit}>
                            <ButtonText>Thêm điểm</ButtonText>
                        </StyledButtonSearch> }

                        {isSubmitting &&  <StyledButtonSearch disable={true}>
                            <ActivityIndicator size="large" color={primary} />
                        </StyledButtonSearch> }


                        <StyledButtonSearch  style={{paddingBottom: 20}} onPress={() => {
                            props.navigation.dispatch(StackActions.replace('Xem_Danh_Sach_SV_Trong_HP',{
                                subjectcode: subjectcode,
                                subjectname: subjectname,
                                teachercode: teachercode,})
                            )}
                        }>
                        
                        <ButtonText>Trở về</ButtonText>
                        </StyledButtonSearch>

                    </StyledFormArea>)}
                </Formik>
            </InnerContainer>
        </>
        </KeyboardAvoidingWrapper>
    );
};

const MyTextInPut = ({label, icon, isPassword, hidePassword, setHidePassword, ...props}) =>{
    return(
        <View style={{paddingTop: 6}}>
            {/* <LeftIcon>
                <Octicons name={icon} size={30} color={brand} />
            </LeftIcon> */}
            <StyledInputLabel>{label}</StyledInputLabel>
            <StyledTextInput_1 {...props} />
            {isPassword && (
                <RightIcon1 onPress={() => setHidePassword(!hidePassword)}>
                    <Ionicons name={hidePassword ? 'md-eye-off' : 'md-eye'}  size={30} color={darkLight} />
                </RightIcon1>
            )}
        </View>
    );
}

export default Them_diem;