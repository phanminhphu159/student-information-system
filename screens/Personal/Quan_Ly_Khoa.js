import React, { Component,useState,useEffect } from "react";
import { StyleSheet, View, Button, Picker, Alert,  ScrollView, Text, TouchableOpacity } from "react-native";
import { Table, TableWrapper, Row, Cell } from 'react-native-table-component';
import{ Colors,StyledButtonSearch,ButtonText} from '../../components/styles'
import Xem_Danh_Sach_Lop from '../../screens/Search/Xem_Danh_Sach_Lop';

import Them_Khoa from '../../screens/Personal/Them_Khoa';
import Sua_Khoa from '../../screens/Personal/Sua_Khoa';

import { StackActions } from '@react-navigation/native';

import axios from 'axios';
var localStorage = require('localStorage');
//Colors
const {tertiary,header_color} = Colors;

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
const Stack = createStackNavigator();
const Quan_Ly_Khoa = () => {
    return(
        // <NavigationContainer>   
            <Stack.Navigator
                
                screenOptions={{
                    headerStyle: {
                        backgroundColor: 'transparent'
                    },
                    headerTintColor: tertiary,
                    headerTransparent: true,
                    headerTitle: '',
                    headerLeftContainerStyle:{
                        paddingLeft: 20
                    },
                    headerLeft: null,
                }}
                initialRouteName="Quan_Ly_Khoa"
            >
                <Stack.Screen name="Them_Khoa" component={Them_Khoa} />
                <Stack.Screen name="Sua_Khoa" component={Sua_Khoa} />
                <Stack.Screen name="Quan_Ly_Khoa" component={Table_Search} />
            </Stack.Navigator>
        // </NavigationContainer>
    );
}

const Table_Search = (props) => {

  const [selectedVal, setSelectedVal] = useState(null);
  const [tableData, setTableData] = useState([]);
  const [isCalled, setCalled] = useState(false);
  
  const state = {
    tableHead: ['TT', 'Tên Khoa','Mã khoa','Sửa'],
    widthArr: [40,180,140, 100],
   }

    const token = localStorage.getItem("token");
    const USER_TOKEN = token ? "Bearer ".concat(token) : null;
    axios.defaults.headers= {Authorization: USER_TOKEN}; 
    useEffect(() => {
    if (USER_TOKEN && !isCalled) {
        // setCalled(true);
      const url = 'http://pbl6-point-lookup.us-east-1.elasticbeanstalk.com/api/findMajor'; 
      axios
            .get(url)
            .then((resp)=> {
              const MyArr = resp.data;
              if(MyArr.data == null){

              }else{
              var Table = MyArr.data.map(record=>([record.majorName,record.majorCode]));
              

              for (let i = 0; i < Table.length; i += 1) {
                Table[i].unshift(i + 1);
              }for (let i = 0; i < Table.length; i += 1) {
                Table[i].push('');
              }


              setTableData(Table);}
        })
        .catch((error) => {
            // console.warn(error);
          })
        }
        }, []);

  const Addmajor = () => {
    props.navigation.navigate("Them_Khoa");
    };

  const Editmajor=() => {
    props.navigation.navigate("Sua_Khoa");
  }
  
 
    const Edit = (data, index) => (
        // Alert.alert(index)
        <TouchableOpacity onPress={() => {
            props.navigation.navigate("Sua_Khoa",{majorcode : tableData[index][2]})}}>

          <View style={styles.btn}>
            <Text style={styles.btnText}>Sửa</Text>
          </View>
        </TouchableOpacity>
      );


      
    const Delete = (data, index) => (
      <TouchableOpacity onPress={() => {
          props.navigation.dispatch(StackActions.replace('Them_diem',{
            subjectcode: subjectcode,
            subjectname: subjectname,
            teachercode: teachercode,
            studentcode: tableData[index][2],
          }
          ))
      }}>
        <View style={styles.btn}>
          <Text style={styles.btnText}>Xóa</Text>
        </View>
      </TouchableOpacity>
    );

    return (
      <View style={styles.container}>
        <View style={{paddingTop: 10}}></View>
        <Text style={{fontSize: 18}}> Danh sách khoa </Text>
        <View style={{paddingTop: 5}}></View>
        <ScrollView horizontal={true}>
            <View style={{height: 425, paddingTop: 5}}> 
            <Table borderStyle={{borderWidth: 1, borderColor: '#C1C0B9'}} style={{paddingTop:5}}>
              <Row data={state.tableHead} widthArr={state.widthArr} style={styles.header} textStyle={styles.text}/>
            </Table>
            <ScrollView style={styles.dataWrapper} >
              <Table 
                borderStyle={{borderWidth: 1, borderColor: '#C1C0B9'}} 
                // style={{height:300, flex: 1}} 
              >
                {   
                  tableData.map((rowData, index) => (
                    <Row
                      key={index}
                      data = {
                          rowData.map((cellData, cellIndex) => (
                            <Cell key={cellIndex} data={cellIndex === 3 ? Edit(cellData, index) : cellData} textStyle={styles.text} style={{alignItems:"center"}}/>
                          ))
                      }
                      widthArr={state.widthArr}
                      style={[styles.row, index%2 && {backgroundColor: '#F7F6E7'}]}
                      textStyle={styles.text}
                    />
                  ))
                }
              </Table>
            </ScrollView>
          </View>
        </ScrollView>
        <View style={{paddingBottom: 60}}>
        <StyledButtonSearch style={{paddingBottom: 20}} onPress={Addmajor}>
        <ButtonText>Thêm khoa</ButtonText>
        </StyledButtonSearch>

        </View>
      </View>
    )
}
 

const styles = StyleSheet.create({
  container: { flex: 1, padding: 16, paddingTop: 13, backgroundColor: '#fff' },
  header: { height: 50, backgroundColor: header_color },
  text: { textAlign: 'center', fontWeight: '100' },
  dataWrapper: { marginTop: -1 },
  row: { height: 40, backgroundColor: '#E7E6E1' },
  btn: { width: 70, height: 20, backgroundColor: '#78B7BB',  borderRadius: 2 },
  btnText: { textAlign: 'center', color: '#fff' },
  container_dropdown: {
    flex:1,
    borderWidth: 1,
    borderColor: "blue",
    borderRadius: 10,
  },
  card: {
    alignItems:'center',
    flexDirection: 'row',
    paddingBottom: 10
  },
});


export default Quan_Ly_Khoa;

