import React, {useState} from 'react';
import { StatusBar } from 'expo-status-bar';

// create components
import {Formik, formik} from 'formik';
import {Octicons, Ionicons, Fontisto} from '@expo/vector-icons';
import {View, ScrollView,Picker,ActivityIndicator}from 'react-native';
import{ StyledContainer, InnerContainer, PageLogo, PageTitle, SubTitle, StyledFormArea, LeftIcon, StyledInputLabel, StyledInputLabel_1 , StyledTextInput, StyledTextInput_1, RightIcon, 
    StyledButton, ButtonText, Colors, MsgBox, Line, ExtraView, ExtraText, TextLink, TextLinkContent, WelcomeContainer, TTSV_Container, WelcomeImage, Avatar, AvatarContainer} from '../../components/styles';

import HomeActivity from '../../screens/Search/test';
import GetUser from '../../components/GetUser';
    
//Colors
const {brand, darkLight, primary} = Colors;
var localStorage = require('localStorage');

// API
import axios from 'axios';
axios.defaults.baseURL = 'http://pbl6-point-lookup.us-east-1.elasticbeanstalk.com/'

// keyboard avoiding view
import KeyboardAvoidingWrapper from '../../components/KeyboardAvoidingWrapper';

const Thong_Tin_Giao_Vien = ({navigation}) => {
    const [hidePassword , setHidePassword] = useState(true);
    const [message, setMessage] = useState();
    const [ messageType, setMessageType] = useState();
    const MyUser = GetUser();

    const handleUpdate= (credentials, setSubmitting) =>{
        HandleMessage(null);
        setSubmitting(false);

        const User_Update = {
            address: credentials.address,
            city: credentials.city,
            email: credentials.email,
            fullName: credentials.fullName,
            phone: credentials.phone,
            userName: MyUser.data.userName,
            // passWord: MyUser.data.passWord,
            // studentCode: credentials.studentCode,
        }
        // const User_Update = {
        //     address: "1234 ád",
        //     city: "Da Nang",
        //     email: "phanminhphu153@gmail.com",
        //     fullName: "Phan Minh Phú",
        //     phone: "0905693228",
        //     userName: "student2"
        // }
        // console.warn(User_Update);
     

        // ----------------------------------------------------------------------------------------
        // axios get token
        const token = localStorage.getItem("token");
        const USER_TOKEN = token ? "Bearer ".concat(token) : null;
        axios.defaults.headers= {Authorization: USER_TOKEN}; // luu vao header


        

        const url = 'http://pbl6-point-lookup.us-east-1.elasticbeanstalk.com/api/updatePerson';
        axios
        .put(url, User_Update, { headers: {"Authorization" : USER_TOKEN} })
        .then((response)=> {
            HandleMessage("Chỉnh sửa thành công")

            })
        .catch((error) => {
            // console.warn(error);
            HandleMessage("Sai cú pháp")})

    }


    const HandleMessage = (message, type= 'FAILED') => {
        setMessage(message);
        setMessageType(type);
    }


    return(
        <KeyboardAvoidingWrapper>
        <>
            <StatusBar style= "darklight" /> 
            <InnerContainer>
                <AvatarContainer resizeMode="cover" source={require('../../assets/img/img3.png')}>
                    <Avatar resizeMode="cover" source={require('../../assets/img/avatar.png')} />
                </AvatarContainer>


                <Formik
                    initialValues = {{fullName: '',avatar:'',city:'', email: '',userName:'', passWord: '', confirmPassword: '', phone: '',studentCode:'', teacherCode: '', address: ''  }}
                    onSubmit = {(values, {setSubmitting}) => {
                        if(values.fullName == '' || values.city == ''|| values.email == ''|| values.phone == ''|| values.teacherCode == ''|| values.address == '') {
                            HandleMessage('Vui lòng điền đủ thông tin');
                            setSubmitting(false); // nhap tu bao nhieu ki tu tro len
                        }else{
                            setTimeout(() => {
                            handleUpdate(values, setSubmitting);
                            }, 0);
                            setSubmitting(false);}
                    }
                }>
                    {({handleChange, handleBlur, handleSubmit, values,isSubmitting}) => (<StyledFormArea>
                        <MyTextInPut 
                            label="Họ và tên"
                            placeholderTextColor= {darkLight}
                            onChangeText = {handleChange('fullName')}
                            onBlur = {handleBlur('fullName')}
                            value= {MyUser === null ? "" : values.fullName != "" && values.fullName != MyUser.data.fullName ? values.fullName : values.fullName = MyUser.data.fullName}
                        />
                        
                        <MyTextInPut editable={false} selectTextOnFocus={false}
                        label="Mã GV "
                        placeholderTextColor= {darkLight}
                        onChangeText = {handleChange('teacherCode')}
                        onBlur = {handleBlur('teacherCode')}
                        value= {MyUser === null ? "": values.teacherCode != "" && values.teacherCode != MyUser.data.teacherCode ? values.teacherCode : MyUser.data.teacherCode == null ? values.teacherCode = 'admin' :values.teacherCode = MyUser.data.teacherCode}
                        />
                        
                        <MyTextInPut
                        label="Thành phố"
                        placeholderTextColor= {darkLight}
                        onChangeText = {handleChange('city')}
                        onBlur = {handleBlur('city')}
                        value= {MyUser === null ? "": values.city != "" && values.city != MyUser.data.city ? values.city :values.city = MyUser.data.city}
                        />
                        
                        <MyTextInPut
                        label="Địa chỉ"
                        placeholderTextColor= {darkLight}
                        onChangeText = {handleChange('address')}
                        onBlur = {handleBlur('address')}
                        value= {MyUser === null ? "": values.address != "" && values.address != MyUser.data.address ? values.address :values.address = MyUser.data.address}
                        />
                        
                        <MyTextInPut
                        label="Số điện thoại"
                        placeholderTextColor= {darkLight}
                        placeholderTextColor= {darkLight}
                        onChangeText = {handleChange('phone')}
                        onBlur = {handleBlur('phone')}
                        value= {MyUser === null ? "": values.phone != "" && values.phone != MyUser.data.phone ? values.phone :values.phone = MyUser.data.phone}
                        />
                        
                        <MyTextInPut
                        label="Ngành" editable={false} selectTextOnFocus={false}
                        placeholderTextColor= {darkLight}
                        // onChangeText = {handleChange('email')}
                        // onBlur = {handleBlur('email')}
                        value = "Công nghệ Thông tin"
                        />
                        
                        <MyTextInPut
                        label="Lớp" editable={false} selectTextOnFocus={false}
                        placeholderTextColor= {darkLight}
                        // onChangeText = {handleChange('email')}
                        // onBlur = {handleBlur('email')}
                        value = "18TCLC_DT2"
                        />
                        
                        <MyTextInPut
                        label="Địa chỉ Email"
                        placeholderTextColor= {darkLight}
                        onChangeText = {handleChange('email')}
                        onBlur = {handleBlur('email')}
                        value= {MyUser === null ? "": values.email != "" && values.email != MyUser.data.email ? values.email :values.email =  MyUser.data.email}
                        />


                        

                        



                        {/* <MyTextInPut
                            label="Password"
                            icon= "lock"
                            placeholder= "* * * * * * * * * *"
                            placeholderTextColor= {darkLight}
                            onChangeText = {handleChange('password')}
                            onBlur = {handleBlur('password')}
                            value={values.password}
                            secureTextEntry={hidePassword}
                            isPassword={true}
                            hidePassword={hidePassword}
                            setHidePassword={setHidePassword}
                        /> */}

                <TTSV_Container>
                    {/* <PageTitle welcome={true}>Welcome! Phu</PageTitle>
                    <SubTitle welcome={true}>Phan Minh Phu</SubTitle>
                    <SubTitle welcome={true}>abc@gmail.com</SubTitle> */}
                    <StyledFormArea>
                        
                        <MsgBox type={messageType}>{message}</MsgBox>
                        
                        <Line />
                        {!isSubmitting &&  <StyledButton onPress={handleSubmit}>
                            <ButtonText>Chỉnh sửa hồ sơ</ButtonText>
                        </StyledButton> }

                        {isSubmitting &&  <StyledButton disable={true}>
                            <ActivityIndicator size="large" color={primary} />
                        </StyledButton> }

                    </StyledFormArea>
                    </TTSV_Container>
                    </StyledFormArea>)}
                </Formik>
            </InnerContainer>

        </>
        </KeyboardAvoidingWrapper>
    );
};

const MyTextInPut = ({label, icon, isPassword, hidePassword, setHidePassword, ...props}) =>{
    return(
        <View>
            {/* <LeftIcon>
                <Octicons name={icon} size={30} color={brand} />
            </LeftIcon> */}
            <StyledInputLabel_1>{label}</StyledInputLabel_1>
            <StyledTextInput_1 {...props} />
            {isPassword && (
                <RightIcon onPress={() => setHidePassword(!hidePassword)}>
                    <Ionicons name={hidePassword ? 'md-eye-off' : 'md-eye'}  size={30} color={darkLight} />
                </RightIcon>
            )}
        </View>
    );
}

export default Thong_Tin_Giao_Vien;