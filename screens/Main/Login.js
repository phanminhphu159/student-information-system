import React, {useState,useEffect} from 'react';
import { StatusBar } from 'expo-status-bar';

// create components
import {Formik, formik, isEmptyArray} from 'formik';
import {Octicons, Ionicons, Fontisto} from '@expo/vector-icons';
import {View, ActivityIndicator} from 'react-native';
import{ StyledContainer, InnerContainer, PageLogo, PageTitle, SubTitle, StyledFormArea, LeftIcon, StyledInputLabel, StyledTextInput, RightIcon, StyledButton, ButtonText, Colors, MsgBox, Line, ExtraView, ExtraText, TextLink, TextLinkContent} from '../../components/styles'


// API
import axios from 'axios';
axios.defaults.baseURL = 'http://pbl6-point-lookup.us-east-1.elasticbeanstalk.com'
// axios.defaults.baseURL = 'http://192.168.1.105:8082/'

//Colors
const {brand, darkLight, primary,darkblue,blue,royalblue} = Colors;
var localStorage = require('localStorage');

// keyboard avoiding view
import KeyboardAvoidingWrapper from '../../components/KeyboardAvoidingWrapper';




const Login = ({navigation}) => {
    const [hidePassword , setHidePassword] = useState(true);
    const [message, setMessage] = useState();
    const [ messageType, setMessageType] = useState();
    const [user, setUser] = useState();

    const handleLogin = (credentials, setSubmitting) =>{
        HandleMessage(null);
     

        // ----------------------------------------------------------------------------------------
        // kiem tra username pass co null hay ko
        if ( credentials.username === null && credentials.password === null) {
                HandleMessage('ERROR');
        }else{
            // axios get token
            const url = 'http://pbl6-point-lookup.us-east-1.elasticbeanstalk.com/api/login'; 
            axios
            .post(url, credentials)
            .then((response)=> {
                localStorage.setItem("token", response.data); // set localstorage
                const token = localStorage.getItem("token");
                const USER_TOKEN = token ? "Bearer ".concat(token) : null;
                axios.defaults.headers= {Authorization: USER_TOKEN}; // luu vao header

                // axios get user by token
                const url1 = 'http://pbl6-point-lookup.us-east-1.elasticbeanstalk.com/api/findPerson'; 
                if (USER_TOKEN) {
                axios
                    .get(url1)
                    .then((resp)=> {
                    setUser(resp.data || {});
                    const myUser = resp.data;
                    if (myUser == {} || myUser == null) {
                        HandleMessage("GET user by token error!");
                    }
                    else{

                        const role = myUser.data.roles.map((item) => {
                            return item.roleCode;
                          });
                        if (role == "STUDENT"){
                            navigation.navigate('Welcome', {roles: "STUDENT", user1: myUser});
                        } 
                        else if(role == "TEACHER"){
                            navigation.navigate('Welcome', {roles: "TEACHER", user1: myUser});
                        }
                        else{
                            navigation.navigate('Welcome', {roles: "ADMIN", user1: myUser});
                        }
                    }
                    
                })
                .catch((error) => {
                    console.warn(error);
                    HandleMessage("GET user by token error!")})
                } 
                else {
                    setUser({});
                }

            })
            .catch((error) => {
                console.warn(error);
                HandleMessage("Sai tên tài khoản hoặc mật khẩu")})


        }
        setSubmitting(false);
    }


    const HandleMessage = (message, type= 'FAILED') => {
        setMessage(message);
        setMessageType(type);
    }

    
    const GetToken = (url, credentials) => {
        setMessage(message);
        setMessageType(type);
    }

    
    return(
        <KeyboardAvoidingWrapper>
            <StyledContainer>
            <StatusBar style="dark" />
            <InnerContainer>
                <PageLogo resizeMode="stretch" source={require('../../assets/img/logo_dut.png')} />
                <View style={{paddingTop: 5}}/>
                <PageTitle> Đăng nhập  </PageTitle>
                {/* <SubTitle>Accoung Login</SubTitle> */}
                <View style={{paddingTop: 5}}/>
            
                <Formik
                    initialValues = {{username: '', password: ''}}
                    onSubmit = {(values, {setSubmitting}) => {
                        // console.log(values);
                        // navigation.navigate("Welcome");
                        if(values.username == '' || values.password == '') {
                            HandleMessage('Vui lòng điền đủ thông tin');
                            setSubmitting(false); // nhap tu bao nhieu ki tu tro len
                        }else{
                            handleLogin(values, setSubmitting);
                        }
                    }}
                >
                    {({handleChange, handleBlur, handleSubmit, values, isSubmitting}) => (<StyledFormArea>
                        <MyTextInPut
                            label="Tài khoản"
                            icon= "mail"
                            placeholder= "abc@gmail.com"
                            placeholderTextColor= {darkLight}
                            onChangeText = {handleChange('username')}
                            onBlur = {handleBlur('username')}
                            value={values.username}
                            keyboardType = "email-address"
                        />

                        <View style={{paddingTop: 5}}/>
                        <MyTextInPut
                            label="Mật khẩu"
                            icon= "lock"
                            placeholder= "* * * * * * * * * *"
                            placeholderTextColor= {darkLight}
                            onChangeText = {handleChange('password')}
                            onBlur = {handleBlur('password')}
                            value={values.password}
                            secureTextEntry={hidePassword}
                            isPassword={true}
                            hidePassword={hidePassword}
                            setHidePassword={setHidePassword}
                        />
                        <View style={{paddingTop: 10}}/>
                        <MsgBox type={messageType}>{message}</MsgBox>
                        <View style={{paddingTop: 10}}/>

                        {!isSubmitting &&  <StyledButton onPress={handleSubmit}>
                            <ButtonText>Đăng nhập</ButtonText>
                        </StyledButton> }

                        {isSubmitting &&  <StyledButton disable={true}>
                            <ActivityIndicator size="large" color={primary} />
                        </StyledButton> }


                        <Line />
                        {/* <StyledButton google={true} onPress={handleSubmit}>
                            <Fontisto name="google" color={primary} size={25} />
                            <ButtonText google={true} >Sign in with Google</ButtonText>
                        </StyledButton> */}
                        <ExtraView>
                            <ExtraText> Bạn chưa có tài khoản ?</ExtraText>
                            <TextLink onPress={() => navigation.navigate("Signup_Student")}>
                                <TextLinkContent> Đăng ký</TextLinkContent>
                            </TextLink>
                        </ExtraView>
                        <View style={{paddingBottom: 20}}/>

                    </StyledFormArea>)}
                </Formik>
            </InnerContainer>
        </StyledContainer>
        </KeyboardAvoidingWrapper> 
    );
};

const MyTextInPut = ({label, icon, isPassword, hidePassword, setHidePassword, ...props}) =>{
    return(
        <View>
            <LeftIcon>
                <Octicons name={icon} size={30} color={royalblue} />
            </LeftIcon>
            <StyledInputLabel>{label}</StyledInputLabel>
            <StyledTextInput {...props} />
            {isPassword && (
                <RightIcon onPress={() => setHidePassword(!hidePassword)}>
                    <Ionicons name={hidePassword ? 'md-eye-off' : 'md-eye'}  size={30} color={darkLight} />
                </RightIcon>
            )}
        </View>
    );
}

export default Login;