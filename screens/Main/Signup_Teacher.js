import React, {useState} from 'react';
import { StatusBar } from 'expo-status-bar';

// create components
import {Formik, formik} from 'formik';
import {Octicons, Ionicons, Fontisto} from '@expo/vector-icons';
import {View, TouchableOpacity, Picker, StyleSheet, Text, Button, Alert,ActivityIndicator} from 'react-native';
import{ StyledContainer,StyledContainer1, InnerContainer, PageLogo, PageTitle, SubTitle, StyledFormArea, LeftIcon, StyledInputLabel, StyledTextInput, RightIcon, StyledButton, ButtonText, Colors, MsgBox, Line, ExtraView, ExtraText, TextLink, TextLinkContent} from '../../components/styles'

//Colors
const {brand, darkLight, primary,royalblue} = Colors;
// API
import axios from 'axios';
axios.defaults.baseURL = 'http://pbl6-point-lookup.us-east-1.elasticbeanstalk.com/'

// Datetimepicker
import DateTimePicker from '@react-native-community/datetimepicker';

// keyboard avoiding view
import KeyboardAvoidingWrapper from '../../components/KeyboardAvoidingWrapper';


class Nganh_Picker extends React.Component{
    constructor(props) {
        super(props)
        this.state = {
            PickerSelectedVal : ''
        }
    }
    
    getSelectedPickerValue=()=>{
      Alert.alert("Selected country is : " +this.state.PickerSelectedVal);
    }
    render()
    {

    return(
    <>
        <StyledInputLabel>Khoa</StyledInputLabel>
        {/* <View style={styles.card}> */}
        <View style={styles.container_dropdown}>
        <Picker
        style={{ height: 45, fontSize: 28}}
        itemStyle={{ backgroundColor: "grey", color: "blue", fontSize:28 }}
        itemTextStyle={{fontSize: 15}}
        activeItemTextStyle={{fontSize: 18, fontWeight: 'bold'}}
        
        selectedValue={this.state.PickerSelectedVal}
        onValueChange={(itemValue, itemIndex) => this.setState({PickerSelectedVal: itemValue})} >

        <Picker.Item label="Công nghệ thông tin" value="asd" />
        <Picker.Item label="USA" value="USA" />
        <Picker.Item label="China" value="China" />
        <Picker.Item label="Russia" value="Russia" />
        <Picker.Item label="United Kingdom" value="United Kingdom" />
        <Picker.Item label="France" value="France" />   
        </Picker>
        {/* </View> */}
        </View>
        <View style={{paddingTop: 10}} >
        <Button s title="Get Selected Picker Value" onPress={ this.getSelectedPickerValue } />
        </View>
    </>
    );}
}



class Lop_Picker extends React.Component{
    constructor(props) {
        super(props)
        this.state = {
            PickerSelectedVal : ''
        }
    }
    
    getSelectedPickerValue=()=>{
      Alert.alert("Selected country is : " +this.state.PickerSelectedVal);
    }
    render()
    {

    return(
    <>
        <StyledInputLabel>Lớp</StyledInputLabel>
        {/* <View style={styles.card}> */}
        <View style={styles.container_dropdown}>
        <Picker
        style={{ height: 45, fontSize: 28}}
        itemStyle={{ backgroundColor: "grey", color: "blue", fontSize:28 }}
        itemTextStyle={{fontSize: 15}}
        activeItemTextStyle={{fontSize: 18, fontWeight: 'bold'}}
        
        selectedValue={this.state.PickerSelectedVal}
        onValueChange={(itemValue, itemIndex) => this.setState({PickerSelectedVal: itemValue})} >

        <Picker.Item label="Công nghệ thông tin" value="asd" />
        <Picker.Item label="USA" value="USA" />
        <Picker.Item label="China" value="China" />
        <Picker.Item label="Russia" value="Russia" />
        <Picker.Item label="United Kingdom" value="United Kingdom" />
        <Picker.Item label="France" value="France" />   
        </Picker>
        {/* </View> */}
        </View>
        <View style={{paddingTop: 10}} >
        <Button s title="Get Selected Picker Value" onPress={ this.getSelectedPickerValue } />
        </View>
    </>
    );}
}


const Signup_Teacher = ({navigation}) => {
    // useState — const [state, setState] = useState(initialState);. Returns a stateful value, and a function to update it. During the initial render, the returned 
    //
    const [hidePassword , setHidePassword] = useState(true);
    const [hidePassword1 , setHidePassword1] = useState(true);
    const [show, setShow] = useState(false);
    const [date, setDate] = useState(new Date(2000, 0, 1));
    const [message, setMessage] = useState();
    const [ messageType, setMessageType] = useState();

    const [dob, setDob] = useState();

    const onChange = (event, selectedDate) => {
        const currenDate = selectedDate || date;
        setShow(false);
        setDate(currenDate);
        setDob(currenDate);
        }
    
    const ShowDatePicker = () => {
        setShow(true);
    }
    

    // form handling
    const handleSignup = (credentials, setSubmitting) =>{
        HandleMessage(null);
        const User_Signup = {
            address: credentials.address,
            avatar: credentials.avatar,
            city: credentials.city,
            email: credentials.email,
            fullName: credentials.fullName,
            passWord: credentials.passWord,
            phone: credentials.phone,
            teacherCode: credentials.teacherCode,
            userName: credentials.userName,
        }
        // console.warn(User_Signup);
     

       
            // axios get token
            const url = 'http://pbl6-point-lookup.us-east-1.elasticbeanstalk.com/api/register'; 
            axios
            .post(url, User_Signup)
            .then((response) => {
                HandleMessage("Đăng ký thành công");
                })
            .catch((error) => {
                // console.warn(error);
                HandleMessage("Đăng ký thất bại")})
            setSubmitting(false);

    }


    const HandleMessage = (message, type= 'FAILED') => {
        setMessage(message);
        setMessageType(type);
    }


    return(
        <KeyboardAvoidingWrapper>
        <View style={{paddingBottom: 50}}>
        <StyledContainer1>
            <StatusBar style="dark" />
            <InnerContainer>
                <PageTitle> Đăng ký  </PageTitle>
                <SubTitle>Đăng ký tài khoản giáo viên</SubTitle>
            
                {show && (
                    <DateTimePicker 
                        testID="dateTimePicker"
                        value = {date}
                        mode = 'date'
                        is24Hour={true}
                        display="default"
                        onChange = {onChange}   
                    />
                )}

                <Formik
                    initialValues = {{fullName: '',avatar:'',city:'', email: '',userName:'', passWord: '', confirmPassword: '', phone: '',teacherCode:'', teacherCode: '', address: ''  }}
                    onSubmit = {(values, {setSubmitting}) => {
                        if(values.fullName == '' || values.city == ''|| values.email == ''|| values.userName == ''|| values.passWord == ''|| values.confirmPassword == '' || values.phone == '' || values.teacherCode == ''|| values.address == '') {
                            HandleMessage('Vui lòng điền đủ thông tin');
                            setSubmitting(false); 
                        }else if(values.passWord != values.confirmPassword){
                            HandleMessage('Sai mật khẩu');
                            setSubmitting(false); 
                        }
                        else{
                            handleSignup(values, setSubmitting);
                        }
                    }}
                >
                    {({handleChange, handleBlur, handleSubmit, values, isSubmitting}) => (<StyledFormArea>
                        <MyTextInPut
                            label="Họ và tên"
                            icon= "person"
                            placeholder= "Phan Minh Phú"
                            placeholderTextColor= {darkLight}
                            onChangeText = {handleChange('fullName')}
                            onBlur = {handleBlur('fullName')}
                            value={values.fullName}
                        />
                        
                        <MyTextInPut
                            label="Mã GV"
                            icon= "person"
                            placeholder= "phanminhphu123"
                            placeholderTextColor= {darkLight}
                            onChangeText = {handleChange('teacherCode')}
                            onBlur = {handleBlur('teacherCode')}
                            value={values.teacherCode}
                        />

                        
                        <MyTextInPut
                            label="Thành phố"
                            icon= "business"
                            placeholder= "Đà Nẵng"
                            placeholderTextColor= {darkLight}
                            onChangeText = {handleChange('city')}
                            onBlur = {handleBlur('city')}
                            value={values.city}
                        />  

                        <MyTextInPut
                            label="Địa chỉ"
                            icon= "business"
                            placeholder= "123 Lê Lợi"
                            placeholderTextColor= {darkLight}
                            onChangeText = {handleChange('address')}
                            onBlur = {handleBlur('address')}
                            value={values.address}
                        />  

                        <MyTextInPut
                            label="Số điện thoại"
                            icon= "phone-portrait-outline"
                            placeholder= "0905693609"
                            placeholderTextColor= {darkLight}
                            onChangeText = {handleChange('phone')}
                            onBlur = {handleBlur('phone')}
                            value={values.phone}
                        />  


                        {/* <Nganh_Picker/>
                        <Lop_Picker/> */}


                        <MyTextInPut
                            label="Địa chỉ Email"
                            icon= "mail"
                            placeholder= "phu123@gmail.com"
                            placeholderTextColor= {darkLight}
                            onChangeText = {handleChange('email')}
                            onBlur = {handleBlur('email')}
                            value={values.email}
                            keyboardType = "email-address"
                        />

                        
                        <MyTextInPut
                            label="Tài khoản"
                            icon= "person"
                            placeholder= "phanminhphu123"
                            placeholderTextColor= {darkLight}
                            onChangeText = {handleChange('userName')}
                            onBlur = {handleBlur('userName')}
                            value={values.userName}
                        />

                        <MyTextInPut
                            label="Mật khẩu"
                            icon= "lock-closed"
                            placeholder= "* * * * * * * * * *"
                            placeholderTextColor= {darkLight}
                            onChangeText = {handleChange('passWord')}
                            onBlur = {handleBlur('passWord')}
                            value={values.passWord}
                            secureTextEntry={hidePassword1}
                            isPassword={true}
                            hidePassword={hidePassword1}
                            setHidePassword={setHidePassword1}
                        />

                        <MyTextInPut
                            label="Nhập lại mật khẩu"
                            icon= "lock-closed"
                            placeholder= "* * * * * * * * * *"
                            placeholderTextColor= {darkLight}
                            onChangeText = {handleChange('confirmPassword')}
                            onBlur = {handleBlur('confirmPassword')}
                            value={values.confirmPassword}
                            secureTextEntry={hidePassword}
                            isPassword={true}
                            hidePassword={hidePassword}
                            setHidePassword={setHidePassword}
                        />

                    

                        <View style={{paddingBottom: 20}}/>
                        <MsgBox type={messageType}>{message}</MsgBox>

                        <Line />
                        {!isSubmitting &&  <StyledButton onPress={handleSubmit}>
                            <ButtonText>Đăng ký</ButtonText>
                        </StyledButton> }

                        {isSubmitting &&  <StyledButton disable={true}>
                            <ActivityIndicator size="large" color={primary} />
                        </StyledButton> }


                    </StyledFormArea>)}
                </Formik>
            </InnerContainer>
        </StyledContainer1>
        </View>
        </KeyboardAvoidingWrapper>
    );
};

const MyTextInPut = ({label, icon, isPassword, hidePassword, setHidePassword, isDate, ShowDatePicker, ...props}) =>{
    return(
        <View>
            <LeftIcon>
                <Ionicons name={icon} size={33} color={royalblue} />
            </LeftIcon>
            <StyledInputLabel>{label}</StyledInputLabel>
            {!isDate && <StyledTextInput {...props} />}
            {isDate && (
                <TouchableOpacity onPress={ShowDatePicker}>
                    <StyledTextInput {...props} />
                </TouchableOpacity>
            )}
            {isPassword && (
                <RightIcon onPress={() => setHidePassword(!hidePassword)}>
                    <Ionicons name={hidePassword ? 'md-eye-off' : 'md-eye'}  size={30} color={darkLight} />
                </RightIcon>
            )}
        </View>
    );
}


const styles = StyleSheet.create({
    container_dropdown: {
      flex:1,
      borderWidth: 1,
      borderColor: "blue",
      borderRadius: 10,
    //   alignContent: 'center',
      height: 50,
    },
    card: {
      height: 60,
      flex:1,
      paddingBottom: 10,
      textAlignVertical: 'center'
    },
  });

export default Signup_Teacher;