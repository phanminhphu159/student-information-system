import React, {useState} from 'react';
import { StatusBar } from 'expo-status-bar';

// create components
import {Formik, formik} from 'formik';
import {Octicons, Ionicons, Fontisto} from '@expo/vector-icons';
import {View, Text} from 'react-native';
import{ StyledContainer, InnerContainer, PageLogo, PageTitle, PageTitle1, SubTitle, StyledFormArea, LeftIcon, StyledInputLabel, StyledTextInput, RightIcon, 
    StyledButton, ButtonText, Colors, MsgBox, Line, ExtraView, ExtraText, TextLink, TextLinkContent, WelcomeContainer, WelcomeImage, Avatar} from '../../components/styles'

const Welcome = ({route,navigation}) => {
    const roles = route.params.roles;
    const user = route.params.user1;
    return(
        <>
            <StatusBar style="light" />
            <InnerContainer>
                <WelcomeImage resizeMode="stretch" source={require('../../assets/img/DUT-logo2.jpg')}/>
                <WelcomeContainer>
                    <PageTitle1 welcome={true}>Welcome!</PageTitle1>
                    <Text style={{fontSize: 25}}>{user.data.fullName}</Text>
                    {/* <SubTitle welcome={true}>abc@gmail.com</SubTitle> */}
                    <View style={{paddingTop: 20}}/>
                    <StyledFormArea>
                    <Avatar resizeMode="cover" source={require('../../assets/img/avatar.png')} />
                        <View style={{paddingTop: 30}}/>
                        <Line />
                        <StyledButton onPress={() => { roles === 'STUDENT' ? navigation.navigate('Bottom_Tabs') : roles === 'TEACHER' ? navigation.navigate('Bottom_Tabs_Teacher') : navigation.navigate('Bottom_Tabs_Admin')}}>
                            <ButtonText>OK</ButtonText>
                        </StyledButton>
                    </StyledFormArea>
                </WelcomeContainer>
            </InnerContainer>
        </>
    );
};

export default Welcome;