var localStorage = require('localStorage');
import axios from 'axios';
axios.defaults.baseURL = 'http://pbl6-point-lookup.us-east-1.elasticbeanstalk.com'
import { useState,useEffect} from "react";


const GetMajor = () => {
    const token = localStorage.getItem("token");
    const USER_TOKEN = token ? "Bearer ".concat(token) : null
    axios.defaults.headers= {Authorization: USER_TOKEN}; ;
    const [major, setMajor] = useState(null)
    const [isCalled, setCalled] = useState(false);
    const url = 'http://pbl6-point-lookup.us-east-1.elasticbeanstalk.com/api/findMajor'; 
    // console.warn(USER_TOKEN);
    useEffect(() => {
    if (USER_TOKEN && !isCalled) {
        setCalled(true);
        axios
            .get(url)
            .then((resp)=> {
            setMajor(resp.data || {});
        })
        .catch((error) => {
            console.warn(error);})
        } 
        // else {
        //     setMajor({});
        // }
    }, []);
    return major;
}



export default GetMajor;