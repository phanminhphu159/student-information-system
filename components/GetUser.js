var localStorage = require('localStorage');
import axios from 'axios';
axios.defaults.baseURL = 'http://pbl6-point-lookup.us-east-1.elasticbeanstalk.com/'
import { useState,useEffect} from "react";


const GetUser = () => {
    const token = localStorage.getItem("token");
    const USER_TOKEN = token ? "Bearer ".concat(token) : null;
    axios.defaults.headers= {Authorization: USER_TOKEN}; 
    const [user, setUser] = useState(null);
    const [isCalled, setCalled] = useState(false);
    const url = 'http://pbl6-point-lookup.us-east-1.elasticbeanstalk.com/api/findPerson'; 
    // console.warn(USER_TOKEN);
    useEffect(() => {
    if (USER_TOKEN && !isCalled) {
        setCalled(true);
        axios
            .get(url)
            .then((resp)=> {
            setUser(resp.data || {});
        })
        .catch((error) => {
            console.warn(error);})
        } 
        // else {
        //     setUser({});
        // }
    }, []);
    return user;
}


export default GetUser;