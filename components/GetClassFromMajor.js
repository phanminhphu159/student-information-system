var localStorage = require('localStorage');
import axios from 'axios';
axios.defaults.baseURL = 'http://pbl6-point-lookup.us-east-1.elasticbeanstalk.com'
import { useState,useEffect} from "react";


const GetClassFromMajor = (majorCode) => {
    const token = localStorage.getItem("token");
    const USER_TOKEN = token ? "Bearer ".concat(token) : null;
    axios.defaults.headers= {Authorization: USER_TOKEN}; 
    const [clas, setClass] = useState(null)
    const [isCalled, setCalled] = useState(false);
    const url = 'http://pbl6-point-lookup.us-east-1.elasticbeanstalk.com/api/findClassByMajor?majorCode='.concat(majorCode); 
    
    // console.warn(USER_TOKEN);
    useEffect(() => {
    if (USER_TOKEN ) {
    // if (USER_TOKEN && !isCalled) {
        // setCalled(true);
        axios
            .get(url)
            .then((resp)=> {
            setClass(resp.data || {});
        })
        .catch((error) => {
            console.warn(error);})
        } 
        // else {
        //     setMajor({});
        // }
    }, []);
    return clas;
}



export default GetClassFromMajor;