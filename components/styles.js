import styled from 'styled-components';
import { View, Text, Image, TextInput, TouchableOpacity } from 'react-native';
import Constants from 'expo-constants';

const StatusBarHeight = Constants.statusBarHeight;

// colors
export const Colors = {
    primary: "#ffffff",
    secondary: "#E5E7EB",
    tertiary: "#1F2937",
    darkLight: "#9CA3AF",
    brand: "#6D28D9",
    green: "#10B981",
    blue: "#00bfff",
    darkblue: "#1e90ff",
    royalblue: "#3486E7",
    red: "#EF4444",
    header_color: "#4690CC"
};

const { primary, secondary, tertiary, darkLight, brand, green, red, blue, darkblue, royalblue,header_color } = Colors;

export const StyledContainer = styled.View `
    flex: 1;
    padding: 25px;
    padding-top: ${StatusBarHeight + 35}px;
    background-color: ${primary};
`;

export const StyledContainer1 = styled.View `
    flex: 1;
    padding: 25px;
    padding-top: ${StatusBarHeight + 10}px;
    background-color: ${primary};
`;

export const InnerContainer = styled.View `
    flex: 1;
    width: 100%;
    align-items: center;
`;

export const WelcomeContainer = styled.View `
    width: 100%;
    align-items: center;
    padding: 25px;
    padding-top: 10px;
    justify-content: center;
`;

export const TTSV_Container = styled.View `
    flex: 1;
    width: 100%;
    align-items: center;
    padding: 25px;
    padding-top: 5px;
    padding-bottom : 70px;
    justify-content: center;
`;

export const PageLogo = styled.Image `
    width: 200px;
    height: 190px;
`;

export const AvatarContainer = styled.View `
    flex: 1;
    height: 20%;
    width: 100%;
    align-items: center;
    background-color: ${blue};
`;

export const Avatar = styled.Image `
    width: 150px;
    height: 150px;
    margin: auto;
    border-radius: 75px;
    border-width: 2px;
    border-color: ${secondary};
    margin-bottom: 10px;
    margin-top: 10px;
`;

export const WelcomeImage = styled.Image `
    height: 34%;
    width: 100%;
`;

export const PageTitle = styled.Text `
    font-size: 30px;
    text-align: center;
    font-weight: bold;
    color: ${royalblue};
    padding: 10px; 

    ${(props) => props.welcome && `
        font-size: 35px;
    `}
`;

export const PageTitle1 = styled.Text `
    font-size: 50px;
    text-align: center;
    font-weight: bold;
    color: ${darkblue};
    padding: 10px; 

    ${(props) => props.welcome && `
        font-size: 50px;
    `}
`;

export const SubTitle = styled.Text `
    font-size: 18px;
    margin-bottom: 20px;
    letter-spacing: 1px;
    font-weight: bold;
    color: ${tertiary};

    ${(props) => props.welcome && `
        margin-bottom: 5px;
        font-weight: normal;
    `}
`;

export const StyledFormArea = styled.View `
    width: 90%;
`;

export const StyledTextInput = styled.TextInput `
    background-color : ${secondary};
    padding: 15px;
    padding-left: 60px;
    padding-right: 55px;
    padding-top: 16px;
    border-radius: 10px; 
    font-size: 18px;
    height: 60px;
    margin-vertical: 3px;
    margin-bottom: 10px;
    color: ${tertiary};
`;

// text-transform: uppercase;
export const StyledTextInput_1 = styled.TextInput `
    background-color : ${secondary};
    padding-left: 25px;
    border-radius: 5px; 
    font-size: 18px;
    text-align: left;
    height: 42px;
    margin-vertical: 3px;
    margin-bottom: 10px;
    color: ${tertiary};
`;

export const StyledInputLabel = styled.Text `
    color: ${royalblue};
    font-size: 13px;
    text-align: left;
`;

export const StyledInputLabel_1 = styled.Text `
    padding-top: 12px; 
    color: ${royalblue};
    font-size: 15px;
    text-align: left;
`;

export const LeftIcon = styled.View `
    left: 15px;
    top: 33px;
    position: absolute;
    z-index: 1;
`;

export const RightIcon = styled.TouchableOpacity  `
    right: 15px;
    top: 37px;
    position: absolute;
    z-index: 1;
`;


export const RightIcon1 = styled.TouchableOpacity  `
    right: 15px;
    top: 32px;
    position: absolute;
    z-index: 1;
`;

export const StyledButton = styled.TouchableOpacity  `
    padding: 15px;
    background-color: ${darkblue};
    justify-content: center;
    align-items: center;
    border-radius: 10px;
    margin-vertical: 5px;
    height: 60px;

    ${(props) => props.google == true && `
        background-color: ${green};
        flex-direction: row;
        justify-content: center;
    `}
`;

export const StyledButtonSearch = styled.TouchableOpacity  `
    padding: 15px;
    background-color: ${darkblue};
    justify-content: center;
    align-items: center;
    border-radius: 10px;
    margin-vertical: 5px;
    height: 45px;

    ${(props) => props.google == true && `
        background-color: ${green};
        flex-direction: row;
        justify-content: center;
    `}
`;

export const ButtonText = styled.Text  `
    color: ${primary};
    font-size: 17px;

    ${(props) => props.google == true && `
        padding: 25px;
    `}
`;

export const MsgBox = styled.Text  `
    text-align: center;
    font-size: 13px;
    color: ${props => props.type == 'SUCCESS' ? green: red};
    padding-bottom : 10px;

`;

export const MsgBox1 = styled.Text  `
    text-align: center;
    font-size: 13px;
    color: ${props => props.type == 'SUCCESS' ? green: red};
    padding-bottom : 5px;

`;

export const Line = styled.View  `
    height: 1px;
    width: 100%;
    background-color: ${darkLight};
    margin-vertical: 10px;
`;

export const ExtraView = styled.View  `
    flex: 1;
    justify-content: center;
    flex-direction: row;
    align-items: center;
    padding: 5px;
`;

export const ExtraText = styled.Text  `
    justify-content: center;
    align-content: center;
    color: ${tertiary};
    font-size: 15px;
`;

export const TextLink = styled.TouchableOpacity  `
    justify-content: center;
    align-items: center;
`;

export const TextLinkContent = styled.Text  `
    color: ${brand};
    font-size: 15px;
`;

export const SafeAreaView = styled.View `
    flex: 1;
    justify-content: center;
    align-items: center;
`;

