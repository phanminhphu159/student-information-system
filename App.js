import React from 'react';


import 'react-native-gesture-handler';

// screens
import RootStack from './navigators/Main/RootStack';
import Welcome from 'D:/React native/Phu_test/screens/Welcome';
import Login from 'D:/React native/Phu_test/screens/Login';
import Signup from 'D:/React native/Phu_test/screens/Signup';
import Bottom_Tabs from './navigators/Main/Bottom_Tabs';
import Bottom_Tabs_Teacher from './navigators/Main/Bottom_Tabs_Teacher';
import Main_Screen from './navigators/Main/Main_Screen';
import Suggestion_Tabs from './navigators/Main/Suggestion_Tabs';
import Personal_Tabs from './navigators/Main/Personal_Tabs';
import HomeActivity from 'D:/React native/Phu_test/screens/Search/test';
import Text_Api from './screens/Main/Test_API';

// export default function App() {
//   return <RootStack />;
// }
const App = () => {
  
  // var localStorage = require('localStorage');
  // localStorage.setItem('myKey', '2');
  return (<RootStack />);
}
export default App;

