import React from 'react';

import { NavigationContainer } from '@react-navigation/native';
import {createDrawerNavigator, DrawerContentScrollView, DrawerItemList} from '@react-navigation/drawer';
// style={{flex:1, justifyContent= 'center', alignItems='center'}}
import {SafeAreaView, View, Text, Image, TouchableOpacity, Animated, ScrollView} from 'react-native';

import Thong_Tin_Sinh_Vien from '../../screens/Personal/Thong_Tin_Sinh_Vien';
import Chuong_Trinh_Dao_Tao from '../../screens/Personal/Chuong_Trinh_Dao_Tao';
import Thoi_Khoa_Bieu from '../../screens/Personal/Thoi_Khoa_Bieu';
import Doi_Mat_Khau from '../../screens/Personal/Doi_Mat_Khau';
import Hoc_Phi from '../../screens/Personal/Hoc_Phi';
import Ket_Qua_Hoc_Tap from '../../screens/Personal/Ket_Qua_Hoc_Tap';
import ExampleThree from '../../screens/Personal/test';
import Tim_Kiem_Khoa_Lop from '../../screens/Search/Tim_Kiem_Khoa_Lop';
import Tim_Thong_Tin_Sinh_Vien from '../../screens/Search/Tim_Thong_Tin_Sinh_Vien';
import { StackActions } from '@react-navigation/native';

import GetUser from '../../components/GetUser';
//Colors
import { Colors } from '../../components/styles'
const { tertiary, primary, secondary } = Colors;

// const scrollY = new Animated.Value(0);
// // const diffClamp = Animated.diffClamp(scrollY,0,45); 
// const translateY = scrollY.interpolate({
//     inputRange:[110,450],
//     outputRange:[0,-45]
// })

const Drawer = createDrawerNavigator();
const CustomDrawer = (props) => {
    const MyUser = GetUser();
    return (
        <View style={{flex: 1}}>
            <DrawerContentScrollView {...props}>
                <View 
                style={{ 
                    flexDirection: 'row', 
                    justifyContent: 'space-between', 
                    padding: 20,
                    alignItems: 'center',
                    backgroundColor: '#f6f6f6',
                    marginBottom: 20,
                }}>
                    <View>
                        <Text>{MyUser === null ? "Minh Phu" : MyUser.data.fullName}</Text>
                        <Text>{MyUser === null ? "phanminhphu159@gmail.com": MyUser.data.email}</Text>
                    </View>
                    <Image source={require('../../assets/img/avatar.png')}
                    style={{width:60, height:60, borderRadius:30}}
                    ></Image>
                </View>
                <DrawerItemList {...props} />
            </DrawerContentScrollView>
            <TouchableOpacity style={{position:'absolute', right:0, 
            left:0, bottom: 50, backgroundColor: '#f6f6f6',
            padding: 20
            }}
            onPress={() => {
                props.navigation.dispatch(
                    StackActions.replace('Login'));
                    }}
            >
                <Text>Đăng xuất</Text>
            </TouchableOpacity>
        </View>
    );
}

const Search_Tabs = () => {
    return (
        // <ScrollView 
        // contentContainerStyle={{ flexGrow: 1}}
        // onScroll={(e) => 
        //     scrollY.setValue(nativeEvent.contentOffset.y)
        // }
        // >
        // <Animated.View 
        // style={{
        //     flexGrow: 1, 
        //     transform:[{translateY:translateY}]
        //     }}
        // >
        // {/* // <NavigationContainer> */}
            <Drawer.Navigator 
                screenOptions={{ 
                    headerShown: true, 
                    headerStyle: {
                        backgroundColor: primary, 
                        elevation: 0,
                        shadowOpacity: 0,
                        height: 70,
                    }, 
                    headerLeft: null,
                    headerStatusBarHeight: 30
                    // headerTitle: ''
                }}
                drawerContent={(props) => <CustomDrawer {...props} />
            }>
                <Drawer.Screen  name="Tìm kiếm khoa, lớp" component={Tim_Kiem_Khoa_Lop }></Drawer.Screen>
                <Drawer.Screen  name="Tìm thông tin SV/GV" component={Tim_Thong_Tin_Sinh_Vien}></Drawer.Screen>
                {/* <Drawer.Screen  name="Danh sách học phần" component={Thoi_Khoa_Bieu}></Drawer.Screen> */}
                {/* <Drawer.Screen  name="Học phí" component={RootStack}></Drawer.Screen>
                <Drawer.Screen  name="Kết quả học tập" component={Ket_Qua_Hoc_Tap}></Drawer.Screen>
                <Drawer.Screen  name="Đổi mật khẩu" component={Doi_Mat_Khau}></Drawer.Screen> */}
            </Drawer.Navigator>
        // {/* // </NavigationContainer>  */}
        // </Animated.View>
        // </ScrollView>
        );
}


export default Search_Tabs;