import React from 'react';

//Colors
import { Colors } from '../../components/styles.js';
const { tertiary, primary } = Colors;


// React navigation
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
// import { createBottomTabNavigator} from '@react-navigation/bottom-tabs';

// import IonicIcon from 'react-native-vector-icons';
// import {Text, Dimensions} from 'react-native';

// screens
import Login from '../../screens/Main/Login';
import Signup_Teacher from '../../screens/Main/Signup_Teacher';
import Singup_Student from '../../screens/Main/Singup_Student';
import Welcome from '../../screens/Main/Welcome';
import About from '../../screens/About';
import Thong_Tin_Sinh_Vien from '../../screens/Personal/Thong_Tin_Sinh_Vien';
import Signup_Student from '../../screens/Main/Singup_Student';
import Bottom_Tabs from '../../navigators/Main/Bottom_Tabs';
import Personal_Tabs from '../../navigators/Main/Personal_Tabs';
import Bottom_Tabs_Teacher from '../../navigators/Main/Bottom_Tabs_Teacher';
import Bottom_Tabs_Admin from '../../navigators/Main/Bottom_Tabs_Admin';
import Search_Tabs from '../../navigators/Main/Search_Tabs';

// const fullScreenWidth = Dimensions.get('window').width;
const Stack = createStackNavigator();
// const Tab = createBottomTabNavigator();

// // API
// import axios from 'axios';

// //


const RootStack = () => {
    return(
        <NavigationContainer>   
            <Stack.Navigator
                
                screenOptions={{
                    headerStyle: {
                        backgroundColor: 'transparent'
                    },
                    headerTintColor: tertiary,
                    headerTransparent: true,
                    headerTitle: '',
                    headerLeft: null,
                    headerLeftContainerStyle:{
                        paddingLeft: 20
                    }
                }}
                initialRouteName="Login"
            >
                <Stack.Screen name="Login" component={Login} />
                {/* <Stack.Screen name="Signup_Teacher" component={Signup_Teacher} /> */}
                <Stack.Screen name="Signup_Student" component={Signup_Student} />
                <Stack.Screen name="Bottom_Tabs" component={Bottom_Tabs} />
                <Stack.Screen name="Bottom_Tabs_Teacher" component={Bottom_Tabs_Teacher} />
                <Stack.Screen name="Bottom_Tabs_Admin" component={Bottom_Tabs_Admin} />
                <Stack.Screen name="Personal_Tabs" component={Personal_Tabs} />
                <Stack.Screen name="Search_Tabs" component={Search_Tabs} />
                <Stack.Screen options={{ headerTintColor: primary }} name="Welcome" component={Welcome} />
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default RootStack;



// function HomeStackScreen() {
//     return(
//         <Stack.Navigator>
//             <Stack.Screen name="Home" component={Home} />
//         </Stack.Navigator>
//     );
// }


// function AboutStackScreen() {
//     return(
//         <Stack.Navigator>
//             <Stack.Screen name="About" component={About} />
//         </Stack.Navigator>
//     );
// }

// function ProfileStackScreen() {
//     return(
//         <Stack.Navigator>
//             <Stack.Screen name="Profile" component={Profile} />
//             <Stack.Screen name="MyInfo" component={MyInfo} />
//         </Stack.Navigator>
//     );
// }

// export default function RootStack1(props) {
//     return (
//         <NavigationContainer>
//             <Tab.Navigator
//                 screenOptions={({route}) =>({
//                     headerTitle: () => <Text>Header</Text>,
//                     tabBarIcon: ({focused, color, size, padding}) => {
//                         let iconName;
//                         if(route.name == 'Home') {
//                             iconName = focused ? 'home' : 'home-outline'
//                         } else if (route.name == 'About'){
//                             iconName= focused ? 'infomation-circle' : 'information-circke-outline'; 
//                         } else if(route.name == 'Profile'){
//                             iconName= focused ? 'person' : 'person-outline';
//                         }

//                         return (<IonicIcon name={iconName} size={size} color={color} style={{paddingBottom: padding}} />);
//                     },
//                 })}
//                 tabBarOptions={{
//                     activeTintColor: 'lightseagreen',
//                     inactiveTintColor: 'grey',
//                     labelStyle: {fontSize: 16},
//                     style: {width: fullScreenWidth},
//                 }}
//                 >
//             <Tab.Screen name="Home" component={HomeStackScreen} />
//             <Tab.Screen name="Profile" component={ProfileStackScreen} />
//             <Tab.Screen name="About" component={AboutStackScreen} />
//             </Tab.Navigator>
//         </NavigationContainer>
//     );
// }