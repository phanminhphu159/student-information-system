import React from 'react';

//Colors
import { Colors } from '../../components/styles.js';
const { tertiary, primary } = Colors;


// React navigation
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
// import { createBottomTabNavigator} from '@react-navigation/bottom-tabs';

// import IonicIcon from 'react-native-vector-icons';
// import {Text, Dimensions} from 'react-native';

// screens
import Login from '../../screens/Login';
import Signup from '../../screens/Signup';
import Welcome from '../../screens/Welcome';
import About from '../../screens/About';
import Thong_Tin_Sinh_Vien from '../../screens/Personal/Thong_Tin_Sinh_Vien';
import Bottom_Tabs from '../../navigators/Main/Bottom_Tabs';

// const fullScreenWidth = Dimensions.get('window').width;
const Stack = createStackNavigator();
// const Tab = createBottomTabNavigator();

const RootStack = () => {
    return(
        <NavigationContainer>   
            <Stack.Navigator
                
                screenOptions={{
                    headerStyle: {
                        backgroundColor: 'transparent'
                    },
                    headerTintColor: tertiary,
                    headerTransparent: true,
                    headerTitle: '',
                    headerLeftContainerStyle:{
                        paddingLeft: 20
                    }
                }}
                // initialRouteName="Bottom_Tabs"
            >
                {/* <Stack.Screen name="Bottom_Tabs" component={Bottom_Tabs} /> */}
                <Stack.Screen name="Login" component={Login} />
                <Stack.Screen name="Signup" component={Signup} />
                <Stack.Screen options={{ headerTintColor: primary }} name="Welcome" component={Welcome} />
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default RootStack;

