

// import { FlatList } from 'react-native-gesture-handler';
import React from 'react';
import {StyleSheet, Text, View, Image, ScrollView,TouchableOpacity, Animated} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Suggestion_Tabs from './Suggestion_Tabs';
import Personal_Tabs from './Personal_Tabs';
import Search_Tabs from './Search_Tabs';
import About from '../../screens/About';
import Welcome from '../../screens/Welcome';



//Colors
import { Colors } from '../../components/styles'
const { tertiary, primary } = Colors;
import KeyboardAvoidingWrapper from '../../components/KeyboardAvoidingWrapper';


const Tab = createBottomTabNavigator();
// const scrollY = new Animated.Value(0);
// const diffClamp = Animated.diffClamp(scrollY,0,45); 
// const translateY = diffClamp.interpolate({
//     inputRange:[0,45],
//     outputRange:[0,-45]
// })


const Bottom_Tabs = ({navigation}) => {
    return (
        // <ScrollView 
        // contentContainerStyle={{ flexGrow: 1}}
        // onScroll={(e) => 
        //     scrollY.setValue(nativeEvent.contentOffset.y)
        // }
        // >
        // <Animated.View 
        // style={{
        //     flexGrow: 1, 
        //     transform:[{translateY:translateY}]
        //     }}
        // >
        // <NavigationContainer>
            <Tab.Navigator
                screenOptions = {{
                    tabBarHideOnKeyboard: true,
                    headerStyle: {
                        backgroundColor: 'transparent'
                    },
                    headerTintColor: tertiary,
                    headerTransparent: true,
                    headerTitle: '',
                    headerShown: false,
                    headerLeft: null,
                    tabBarShowLabel: false,
                    tabBarStyle: {
                        ...styles.tabbar,
                        // ...styles.shadow
                    },
                    // tabBarVisibilityAnimationConfig: true, 
                    
                }}
                initialRouteName = "Personal_Tabs"
            >

                

                <Tab.Screen name="Search_Tabs" component={Search_Tabs} 
                options={{
                    tabBarIcon:({focused}) => (
                        <View style={{alignItems:'center', justifyContent: 'center', top: 2}}>
                            <Image 
                                source={require('../../assets/img/Search.png')}
                                resizeMode="contain"
                                style={{
                                    width: 25,
                                    height: 25,
                                    tintColor: focused  ? '#e32f45' : '#748c94'
                                }}
                            />
                            <Text style={{color: focused  ? '#e32f45' : '#748c94', fontSize: 15 }}>
                            Tìm Kiếm
                            </Text>
                        </View>
                    ),
                }}  
                />    
                
                <Tab.Screen name="Personal_Tabs" component={Personal_Tabs}  
                //     options={({ route }) => ({tabBarVisible: this.getTabBarVisibility(route)})
                // } 
                options={{
                    tabBarIcon:({focused}) => (
                        <View style={{alignItems:'center', justifyContent: 'center', top: 2}}>
                            <Image 
                                source={require('../../assets/img/Personal.png')}
                                resizeMode="contain"
                                style={{
                                    width: 25,
                                    height: 25, 
                                    tintColor: focused  ? '#e32f45' : '#748c94'
                                }}
                            />
                            <Text style={{color: focused  ? '#e32f45' : '#748c94', fontSize: 15 }}> 
                            Cá nhân
                            </Text>
                        </View>
                    ),
                }}  
                />
                
                <Tab.Screen name="Suggestion_Tabs" component={Suggestion_Tabs}
                options={{
                    tabBarIcon:({focused}) => (
                        <View style={{alignItems:'center', justifyContent: 'center', top: 2}}>
                            <Image source={require('../../assets/img/Suggestion.png')}
                                resizeMode="contain"
                                style={{
                                    width: 25,
                                    height: 25,
                                    tintColor: focused  ? '#e32f45' : '#748c94',
                                }}
                            />
                            <Text style={{color: focused  ? '#e32f45' : '#748c94', fontSize: 15 }}>
                             Đề Nghị
                            </Text>
                        </View>
                    ),
                }}  
                />
                {/* <Tab.Screen name="About1" component={About}
                options={{
                    tabBarIcon:({focused}) => (
                        <View style={{alignItems:'center', justifyContent: 'center', top: 2}}>
                            <Image 
                                source={require('../../assets/img/Reflect.png')}
                                resizeMode="contain"
                                style={{
                                    width: 25,
                                    height: 25,
                                    tintColor: focused  ? '#e32f45' : '#748c94'
                                }}
                            />
                            <Text style={{color: focused  ? '#e32f45' : '#748c94', fontSize: 15 }}>
                            Phản ánh
                            </Text>
                        </View>
                    ),
                }}   
                />    */}
            </Tab.Navigator>
        // </NavigationContainer>
        // </Animated.View>
        // </ScrollView>
    );
}


const styles = StyleSheet.create({
    tabbar:{
        flex: 1,
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        elevation: 0,
        backgroundColor: primary,
        borderRadius: 0,
        height: 50
    },
    shadow: {
        shadowColor: '#7F5DF0',
        shadowOffset: {
            width:0,
            height:10,
        },
        shadowOpacity: 0.25,
        shadowRadius:3.5,
        elevation:5
    },
    container: {
        marginTop: 32,
        padding: 16,
        minHeight: "100%",
    },
});

export default Bottom_Tabs;
