import React from 'react';

import { NavigationContainer } from '@react-navigation/native';
import {createDrawerNavigator, DrawerContentScrollView, DrawerItemList} from '@react-navigation/drawer';
// style={{flex:1, justifyContent= 'center', alignItems='center'}}
import {SafeAreaView, View, Text, Image, TouchableOpacity, Animated, ScrollView} from 'react-native';

import Thong_Tin_Giao_Vien from '../../screens/Personal/Thong_Tin_Giao_Vien';
import Danh_sach_mon_hoc from '../../screens/Personal/Danh_sach_mon_hoc';
import Thoi_Khoa_Bieu from '../../screens/Personal/Thoi_Khoa_Bieu';
import Doi_Mat_Khau from '../../screens/Personal/Doi_Mat_Khau';
import Quan_Ly_Lop from '../../screens/Personal/Quan_Ly_Lop';
import Quan_Ly_Khoa from '../../screens/Personal/Quan_Ly_Khoa';
import Signup_Teacher from '../../screens/Main/Signup_Teacher';
import { StackActions } from '@react-navigation/native';

import GetUser from '../../components/GetUser';

//Colors
import { Colors } from '../../components/styles'
const { tertiary, primary, secondary } = Colors;

// const scrollY = new Animated.Value(0);
// // const diffClamp = Animated.diffClamp(scrollY,0,45); 
// const translateY = scrollY.interpolate({
//     inputRange:[110,450],
//     outputRange:[0,-45]
// })

const Drawer = createDrawerNavigator();
const CustomDrawer = (props) => {
    const MyUser = GetUser();
    
    return (
        <View style={{flex: 1}}>
            <DrawerContentScrollView {...props}>
                <View 
                style={{ 
                    flexDirection: 'row', 
                    justifyContent: 'space-between', 
                    padding: 20,
                    alignItems: 'center',
                    backgroundColor: '#f6f6f6',
                    marginBottom: 20,
                }}>
                    <View>
                        <Text>{MyUser === null ? "Minh Phu" : MyUser.data.fullName}</Text>
                        <Text>{MyUser === null ? "phanminhphu159@gmail.com": MyUser.data.email}</Text>
                    </View>
                    <Image source={require('../../assets/img/avatar.png')}
                    style={{width:60, height:60, borderRadius:30}}
                    ></Image>
                </View>
                <DrawerItemList {...props} />
            </DrawerContentScrollView>
            <TouchableOpacity style={{position:'absolute', right:0, 
            left:0, bottom: 50, backgroundColor: '#f6f6f6',
            padding: 20
            }}
            onPress={() => {
                props.navigation.dispatch(StackActions.replace('Login'));
                }}>
                <Text>Đăng xuất</Text>
            </TouchableOpacity>
        </View>
    );
}

const Personal_Tabs_Admin = () => {
    return (
            <Drawer.Navigator 
                screenOptions={{ 
                    headerShown: true, 
                    headerStyle: {
                        backgroundColor: primary, 
                        elevation: 0,
                        shadowOpacity: 0,
                        height: 70,

                    }, 
                    headerLeft: null,
                    headerStatusBarHeight: 30
                    // headerTitle: ''
                }}
                drawerContent={(props) => <CustomDrawer {...props} />
            }>
                <Drawer.Screen  name="Thông tin cá nhân " component={Thong_Tin_Giao_Vien}></Drawer.Screen>
                {/* <Drawer.Screen  name="Danh sách môn học" component={Danh_sach_mon_hoc}></Drawer.Screen> */}
                <Drawer.Screen  name="Quản lý lớp" component={Quan_Ly_Lop}></Drawer.Screen>
                <Drawer.Screen  name="Quản lý khoa" component={Quan_Ly_Khoa}></Drawer.Screen>
                <Drawer.Screen  name="Đăng ký tài khoản GV" component={Signup_Teacher}></Drawer.Screen>
                <Drawer.Screen  name="Đổi mật khẩu" component={Doi_Mat_Khau}></Drawer.Screen>
            </Drawer.Navigator>
        // {/* // </NavigationContainer>  */}
        // </Animated.View>
        // </ScrollView>
        );
}


export default Personal_Tabs_Admin;