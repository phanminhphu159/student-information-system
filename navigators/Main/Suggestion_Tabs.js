import React from 'react';

import { NavigationContainer } from '@react-navigation/native';
import {createDrawerNavigator, DrawerContentScrollView, DrawerItemList} from '@react-navigation/drawer';
// style={{flex:1, justifyContent= 'center', alignItems='center'}}
import {SafeAreaView, View, Text, Image, TouchableOpacity} from 'react-native';

import Thong_Tin_Sinh_Vien from '../../screens/Personal/Thong_Tin_Sinh_Vien';
import Chuong_Trinh_Dao_Tao from '../../screens/Personal/Chuong_Trinh_Dao_Tao';
import Thoi_Khoa_Bieu from '../../screens/Personal/Thoi_Khoa_Bieu';
import Hoc_Phi from '../../screens/Personal/Hoc_Phi';
import Ket_Qua_Hoc_Tap from '../../screens/Personal/Ket_Qua_Hoc_Tap';
import Cap_Van_Ban_Xac_Nhan  from '../../screens/Suggestion/Cap_Van_Ban_Xac_Nhan';
import Cap_The_Sinh_Vien  from '../../screens/Suggestion/Cap_The_Sinh_Vien';
import { StackActions } from '@react-navigation/native';

import GetUser from '../../components/GetUser';

const Drawer = createDrawerNavigator();
const CustomDrawer = (props) => {
    const MyUser = GetUser();
    return (
        <View style={{flex: 1}}>
            <DrawerContentScrollView {...props}>
                <View 
                style={{ 
                    flexDirection: 'row', 
                    justifyContent: 'space-between', 
                    padding: 20,
                    alignItems: 'center',
                    backgroundColor: '#f6f6f6',
                    marginBottom: 20,
                }}>
                    <View>
                        <Text>{MyUser === null ? "Minh Phu" : MyUser.data.fullName}</Text>
                        <Text>{MyUser === null ? "phanminhphu159@gmail.com": MyUser.data.email}</Text>
                    </View>
                    <Image source={require('../../assets/img/avatar.png')}
                    style={{width:60, height:60, borderRadius:30}}
                    ></Image>
                </View>
                <DrawerItemList {...props} />
            </DrawerContentScrollView>
            <TouchableOpacity style={{position:'absolute', right:0, 
            left:0, bottom: 50, backgroundColor: '#f6f6f6',
            padding: 20
            }}
            
            onPress={() => {
                props.navigation.dispatch(
                    StackActions.replace('Login'));
                    }}
                    >
                <Text>Đăng xuất</Text>
            </TouchableOpacity>
        </View>
    );
}

const DrawerNavigator = () => {
    return (
        // <NavigationContainer>
            <Drawer.Navigator 
                screenOptions={{ 
                    headerShown: true, 
                    headerStyle: {
                        backgroundColor:'transparent', 
                        elevation: 0,
                        shadowOpacity: 0,
                        height: 70
                    }, 
                    // headerTitle: '',
                    headerStatusBarHeight: 30,
                    headerLeft: null
                }}
                drawerContent={(props) => <CustomDrawer {...props} />
            }>
                <Drawer.Screen  name="Cấp văn bản xác nhận" component={Cap_Van_Ban_Xac_Nhan}></Drawer.Screen>
                <Drawer.Screen  name="Cấp lại thẻ sinh viên" component={Cap_The_Sinh_Vien}></Drawer.Screen>
                {/* <Drawer.Screen  name="Xét hoãn đóng học phí" component={Thoi_Khoa_Bieu}></Drawer.Screen> */}
                {/* <Drawer.Screen  name="test" component={RootStack}></Drawer.Screen> */}
            </Drawer.Navigator>
        // </NavigationContainer>
        );
}

const Suggestion = ({props}) => {
    return(
        // <SafeAreaView>
            
        //     <Text>Suggestion</Text>
        // </SafeAreaView>
        <DrawerNavigator></DrawerNavigator>
    );
};

export default Suggestion;